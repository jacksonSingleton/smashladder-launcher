
Smashladder Dolphin Launcher!
=============================


##Features


Login

![Site Login](./screenshots/login.png)

Files

![Set Dolphin Files](./screenshots/setfiles.png)

Graceful Reconnection Attempts

![Reconnection](./screenshots/disconnects.png)

Deauthorization when user is logged into more than one instance

Launch Dolphin!

![Launch Dolphin!](./screenshots/opendolphin.png)