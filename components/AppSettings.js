"use strict";
const settings = require('electron-settings');

var defaultUserAuthentication = {
    player: null,
    session: null
};
settings.defaults({
    allDolphinBuilds: {},
    userAuthentication: defaultUserAuthentication,
    autoUpdates: true,
    lastLaunch: {
        launch: "SUPER SMASH BROS. Melee (GALE01, Revision 2)",
        game: "Melee"
    },
    shiningStats: {
        active: false
    }
});

class AppSettingsWrapper
{
    static set(name, data){
        return Promise.resolve(settings.setSync(name, data));
    }
    static get(name){
        return Promise.resolve(settings.getSync(name));
    }
    static getSync(name){
        return settings.getSync(name);
    }
}

settings.on('write',(e)=>{
    // console.log('WRITING');
});
settings.on('create',(e)=>{
    console.log('created');
});
module.exports = AppSettingsWrapper;