"use strict";

const FormData = require('form-data');
const constants = require('./constants');
const fetch = require('node-fetch');

const settings = require('./AppSettings');


class Authentication{
    constructor(player, session){
        this.session = session;
        this.player = player;
        if(!this.player || !this.player.id)
        {
            throw {data: player, error: {no_player: true} }
        }
        if(!this.session || !this.session.id)
        {
            throw {data: session, error: {no_player: true} };
        }
        this.authenticated = false;
    }

    isAuthenticated(){
        return this.authenticated;
    }

    checkAuthentication(){
        var SmashladderApi = require("./SmashladderApi.js");

        if(this.checkValid)
        {
            console.log('[SHORTCUT CHECK]');
            return Promise.resolve(this);
        }

        if(this.session)
        {
            var data = {};
            data.session_player_id = this.player.id;
            data.session_key = this.session.token;
        }
        else
        {
            throw {no_player:true};
        }

        return SmashladderApi.post(constants.getUrl(constants.apiEndpoints.AUTHENTICATE_SESSION), data)
            .then(response => {
                var contentType = response.headers.get("content-type");
                if (contentType && contentType.indexOf("application/json") !== -1) {
                    return response.json();
                }
                else {

                    return {success: false, has_cache: true, server_error: true};
                }
            }).then(response => {
                if(response.success)
                {
                    this.authenticated = true;
                    this.checkValid = true;
                    setTimeout(()=>{
                        this.checkValid = false;
                    },60000);
                    constants.player = this.player;
                    return this;
                }
                else
                {
                    constants.player = this.player;
                    this.authenticated = false;
                    if(response.has_cache)
                    {
                        return this;
                    }
                    throw {authentication_failure : true};
                }
            }).catch(error=>{
                throw error;
            });
    }
    static load(){
        return settings.get('userAuthentication')
            .then((authentication) =>{
                if(!authentication)
                {
                    authentication = {};
                }
                return new Authentication(authentication.player, authentication.session);
            }).catch(function(error){
                throw error;
            });
    }
    save(){

        var authenticationData = {
            player: this.player,
            session: this.session
        };

        return settings.set('userAuthentication', authenticationData)
            .then(() =>{
                return authenticationData;
            }).catch(error=>{
                throw error;
            });
    }
    static deleteAuthentication(){
        return settings.set('userAuthentication', {})
            .catch(function(error){
                throw error;
            });
    }
}
var cachedAuthentication = null;

module.exports = Authentication;