
const BuildLaunchSmartDolphin = require('./BuildLaunchSmartDolphin');
const BuildLaunchAhk = require('./BuildLaunchAhk');
const os = require('os');
var DolphinActions = require("./DolphinActions.js");
const Notification = require('./Notification');
const delay = require('timeout-as-promise');
const constants = require('./constants');
const SmashladderApi = require('./SmashladderApi');

class Build
{
    constructor(build){
        this.isSmartDolphin = null;
        for(var property in build){
            if(build.hasOwnProperty(property))
            {
                this[property] = build[property];
            }
        }

        this.hasNewElement = false;
        this.ignoreNextHostMessage = false;
        this.builds = null;
        this.isTesting = false;
    }

    setBuilds(builds){
        this.builds = builds;
    }

    getElement(){
        if(this.element)
        {
            return this.element;
        }
        this.hasNewElement = true;
        return this.element = this.builds.getTemplateElementForBuild(this);
    };
    test(){
        this.getElement().addClass('is_testing');
        this.isTesting = true;

        this.updateElement();

        return new Promise((resolve, reject) => {
            console.log('called test?');
            var finished = ()=>{
                resolve();
                this.isTesting = false;
                this.updateElement();
                Notification.showFinishedTestNotification();

                let build = this;

                var data = {};
                data.builds = {};
                data.builds[build.id] = build.syncSerialize();
                data.builds = JSON.stringify(data.builds);
                SmashladderApi.post(constants.getUrl(constants.apiEndpoints.SYNC_BUILDS), data)
                    .then((response)=>{
                        console.log(response);
                    })
                    .catch((error)=>{
                        console.error(error);
                    });
            };
            delay(1000)
                .then(()=>{
                    console.log('launch');
                    return this.host(null, true)
                }).then(()=>{
                    console.log('here');
                    delay(3000).then(()=>{
                        console.log('KILL CHILD');
                        BuildLaunchSmartDolphin.killChild()
                            .then(()=>{
                                finished();
                            }).catch(()=>{
                            finished();
                        });
                        if(!this.isSmartDolphin)//Never received a message from dolphin
                        {
                            this.changeIsSmartDolphin(false);
                        }
                    });
                }).catch((error)=>{
                    console.log('CAUGHT ERROR', error);
                    if(!this.isSmartDolphin)//Never received a message from dolphin
                    {
                        this.changeIsSmartDolphin(false);
                    }
                    finished();
                });
        });
    };

    exit(){
        BuildLaunchSmartDolphin.killChild();
    }

    host(gameLaunch, closePrevious){
        var parameters = [];
        let launcher = Build.getLauncher(this);
        launcher.closePrevious = closePrevious;

        if(!gameLaunch)
        {
            var settings = require('./AppSettings');
            gameLaunch = settings.getSync('lastLaunch');
        }
        return launcher.host(this, gameLaunch)
            .catch((error)=>{
                console.error(error);
            });
    }
    
    join(hostCode, closePrevious){
        var parameters = [];
        let launcher = Build.getLauncher(this);
        launcher.closePrevious = closePrevious;

        return launcher.join(this, hostCode)
            .catch((error)=>{
                console.error(error);
            });
    }

    startGame(){
        const launcher = Build.getLauncher(this);
        if(launcher && launcher.startGame)
        {
            launcher.startGame();
        }
        else
        {

        }
    }

    resetIsSmartDolphin(){
        this.isSmartDolphin = null;
        this.updateElement();
    }

    ignoreNextHost(value){
        this.ignoreNextHostMessage = value;
    }

    changeIsSmartDolphin(value){
        if(this.isSmartDolphin === null)
        {
            //WE ONLY DO THIS ONCE
            this.isSmartDolphin = value;
            this.save();
            this.updateElement();
        }
    }

    update(newData){
        var somethingChanged = false;
        for(var i in newData){
            if(newData.hasOwnProperty(i))
            {
                if(this[i] !== newData[i])
                {
                    somethingChanged = true;
                }
                this[i] = newData[i];
            }
        }
        return somethingChanged;
    }

    remove(){
        if(this.element)
        {
            this.element.remove();
        }
        this.builds = null;
    }

    syncSerialize(){
        var serialized = {};
        serialized.id = this.dolphin_build_id;
        serialized.is_smart = this.isSmartDolphin;
        serialized.ladder_id = this.ladder_id;
        serialized.launcher_active = this.local_path? 1: 0;
        return serialized;
    }

    saveSerialize(){
        var serialized = {};
        var excludedProperties = {
            'builds':1,
            'element':1,
            'hasNewElement':1,
            'host_code':1,
        };
        for(var i in this)
        {
            if(!this.hasOwnProperty(i))
            {
                continue;
            }
            if(excludedProperties[i])
            {
                continue;
            }
            serialized[i] = this[i];
        }
        return serialized;
    }

    setPath(path){
        this.local_path = path;
        this.isSmartDolphin = null;
    }
    clearPath(){
        this.local_path = null;
        this.isSmartDolphin = null;

    }
    save(){
        if(!this.builds)
        {
            throw 'Builds are required in order to call save from this element';
        }
        return this.builds.save();
    }
    
    updateElement(){
        var build = this;
        var element = this.getElement();

        if(this.isSmartDolphin === true)
        {
            element.addClass('is_smart_dolphin').removeClass('is_dumb_dolphin');
        }
        else if(this.isSmartDolphin === false)
        {
            element.addClass('is_dumb_dolphin').removeClass('is_smart_dolphin');
        }
        else
        {
            element.removeClass('is_dumb_dolphin is_smart_dolphin');
        }

        if(this.download_file)
        {
            element.addClass('has_download')
        }
        else
        {
            element.removeClass('has_download')
        }

        if(this.isTesting)
        {
            element.addClass('is_testing');
        }
        else
        {
            element.removeClass('is_testing');
        }

        var clearButton = element.find('.remove');
        var launchBuild = element.find('.test-launch');
        var hostCode = element.find('.host_code');

        var buildEnabledCheckbox = element.find('.build_toggle');
        var setLocationButton = element.find('.select');

        if(build.local_path)
        {
            element.find('input[name=file_location]').val(build.local_path);
            element.addClass('has_path');
            setEnabled(clearButton, launchBuild, hostCode);
        }
        else
        {
            element.find('input[name=file_location]').val('');
            element.removeClass('has_path');
            setDisabled(clearButton, launchBuild, hostCode)
        }

        hostCode.text(build.host_code ? build.host_code : '');

        function setEnabled(...elements)
        {
            for(var i = 0; i< elements.length; i++)
            {
                elements[i].addClass('enabled').removeClass('disabled').prop('disabled', false);
            }
        }
        function setDisabled(...elements)
        {

            for(var i = 0; i< elements.length; i++)
            {
                elements[i].removeClass('enabled').addClass('disabled').prop('disabled', true);
            }
        }


        if(build.active && !build.isTesting)
        {
            setEnabled(element, setLocationButton);
            buildEnabledCheckbox.prop('checked', true).prop('disabled', false);

        }
        else
        {
            setDisabled(element, setLocationButton, launchBuild, clearButton);
            buildEnabledCheckbox.prop('checked', false).prop('disabled', false);
        }
        buildEnabledCheckbox.data('build_preference_id', build.id);
        element.find('.heading_text').text(build.name);

        element.data('build', build);
        // element.find('select').material_select();
    }

    hosted(hostCode){
        DolphinActions.call('host_code', this, hostCode);
    }

    static getLauncher(build){
        var launcher = null;
        if(!build.local_path)
        {
            throw {error: 'Dolphin Location not set for ' + build.name, build:build};
        }
        if(build.isSmartDolphin || build.isSmartDolphin === null)
        {
            launcher = BuildLaunchSmartDolphin;
        }
        else if(build.isSmartDolphin === false)
        {
            if(os.platform() == 'win32')
            {
                launcher = BuildLaunchAhk;
            }
        }
        if(!launcher)
        {
            throw {error: 'No Launcher Available!', build:build};
        }
        return launcher;
    }
}

module.exports = Build;