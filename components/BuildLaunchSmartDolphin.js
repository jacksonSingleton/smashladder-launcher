"use strict";

const child = require('child_process');
const constants = require("./constants");
const SmashladderApi = require("./SmashladderApi.js");
const Builds = require("./Builds.js");
const DolphinResponse = require("./DolphinResponse.js");
const DolphinActions = require("./DolphinActions.js");
const Notification = require("./Notification.js");
const EventEmitter = require('events');

const DolphinChecker = require('./DolphinChecker');

class BuildLaunchSmartDolphin extends EventEmitter{
    constructor(){
        super();
        this.child = null;

        this.childOpen = true;

        this.mainWindow = null;
        this.closePrevious = null;
    }

    launch(build, parameters, closePrevious){
        if(closePrevious)
        {
            if(!this.child)
            {
                if(DolphinChecker.dolphinIsRunning())
                {
                    Notification.showFocusedAlert('Conflict!', 'Dolphin is already opened. Please close all instances of dolphin!');
                    return Promise.reject(new Error('Dolphin was already opened'));
                }
            }
            return this.killChild()
                    .then(() => {
                        return this.launchChild(build, parameters);
                    });
        }
        else
        {
            return Promise.resolve(this.launchChild(build, parameters));
        }
    }

    host(build){
        let parameters = [];
        if(build.isSmartDolphin)
        {
            parameters.push(["--netplay=" + "host"]);
            parameters.push(["--playercount=2"]);
        }

        return this.launchChild(build, parameters);
    }
    join(build, hostCode){
        let parameters =[];
        if(build.isSmartDolphin)
        {
            parameters.push(["--netplay=" + hostCode]);
        }
        return this.launchChild(build, parameters);
    }

    launchChild(build, parameters){
        var sendData = {};
        if(!parameters)
        {
            parameters = [];
        }
        if(build.hostId)
        {
            sendData = {host_id: build.hostId};
        }

        if(!build.local_path && build.element)
        {
            Notification.showFocusedAlert('Launch Failure', 'Attempted to launch '+build.name + ' but the path is not set!')
                .then()
                .catch(error=>{
                    console.error('hm?');
                });
            build.element.find('input[name=file_location]').addClass('path_not_set invalid');
            sendData.success = 0;
            SmashladderApi.post(constants.getUrl(constants.apiEndpoints.OPENED_DOLPHIN), sendData);
        }

        if(!build.local_path || this.child)
        {
            //Only one child allowed at a time, may consider throwing an error instead
            return Promise.resolve();
        }

        this.child = child.spawn(build.local_path, parameters);

        sendData.success = 1;
        if(!build.isTesting)
        {
            SmashladderApi.post(constants.getUrl(constants.apiEndpoints.OPENED_DOLPHIN), sendData);
        }
        // console.log('[PID]',this.child.pid);
        if(build.element)
        {
            this.emit('launched');
            build.element.addClass('opened').removeClass('highlighted');
            build.element.closest('.builds').addClass('launched_a_dolphin');
        }


        this.child.stdout.on('data', (data) => {
            console.log('stdout: ' + data);
            if(!data)
            {
                console.log('Empty?');
                return;
            }
            var strings = data.toString().split(/\r?\n/);
            for(var i in strings)
            {
                if(!strings.hasOwnProperty(i))
                {
                    continue;
                }
                var stdout = strings[i];
                if(!stdout)
                {
                    continue;
                }

                var result = DolphinResponse.parse(stdout);
                if(DolphinActions.isCallable(result.action))
                {
                    build.changeIsSmartDolphin(true);
                    if(!build.isTesting)
                    {
                        DolphinActions.call(result.action, build, result.value)
                    }
                }
                else
                {
                    console.warn('Unusable stdout', result);
                }
            }
        });

        var hostId = build.hostId;

        this.childKilled = new Promise((resolve, reject) => {
            this.child.on('close', (e)=>{

                console.log('[exit code]', e);
                if(e == 4294967295)
                {
                    build.changeIsSmartDolphin(false);
                }

                if(build.element)
                {
                    build.element.removeClass('opened');
                    build.element.closest('.builds').removeClass('launched_a_dolphin');
                    build.element.removeClass('highlighted');
                }

                if(!build.isTesting)
                {
                    SmashladderApi.post(constants.getUrl(constants.apiEndpoints.CLOSED_DOLPHIN), {host_id: hostId});
                }
                this.child = null;
                resolve();
            });
        });

        return Promise.resolve(this.child);
    }

    killChild(){
        if(this.child)
        {
            this.child.kill();
            return this.childKilled;
        }
        else
        {
            return Promise.resolve();
        }
    }

}


module.exports = new BuildLaunchSmartDolphin();