const $ = require('jquery');
const settings = require('./AppSettings.js');
var constants = require("./constants.js");
const SmashladderApi = require("./SmashladderApi.js");
const Build = require("./Build.js");
const Notification = require("./Notification.js");
const LadderGame = require("./LadderGame.js");
const LadderMap = require("./helpers/LadderMap.js");

class Builds
{
    constructor(builds){
        this.builds = {};
        Builds.instance = this;
        
        for(var i in builds)
        {
            if(!builds.hasOwnProperty(i))
            {
                continue;
            }
            var build = builds[i];
            this.builds[i] = this.createNewBuild(build);
        }
    }

    createNewBuild(buildData){
        var build = new Build(buildData);
        build.setBuilds(this);
        return build;
    }

    updateFromServer(serverBuilds){
        var i;
        console.log('[updating with]', serverBuilds);
        for(i in serverBuilds)
        {
            if(!serverBuilds.hasOwnProperty(i))
            {
                continue;
            }
            var build = serverBuilds[i];
            if(this.builds[build.dolphin_build_id])
            {
                this.builds[build.dolphin_build_id].update(build);
            }
            else
            {
                this.builds[build.dolphin_build_id] = this.createNewBuild(build);
            }
        }
        for(i in this.builds){
            if(!this.builds.hasOwnProperty(i))
            {
                continue;
            }
            if(!serverBuilds[i])
            {
                var oldElement = this.builds[i];
                delete this.builds[i];
                oldElement.remove();
            }
        }
    }

    highlightBuild(name){
        if(!this.hasBuild[name])
        {
            return;
        }
        for(var i in this.builds)
        {
            if(!this.builds.hasOwnProperty(i))
            {
                continue;
            }
            var build = this.builds[i];
            if(!build || !build.element)
            {
                continue;
            }
            if(build.element.hasClass('opened'))
            {
                continue;
            }
            if(i == name)
            {
                build.element.removeClass('highlighted');
            }
            else
            {
                build.element.addClass('highlighted');
            }
        }
    }

    getBuilds(){
        return this.builds;
    }

    hasBuild(name){
        return this.builds.hasOwnProperty(name);
    }

    save(){
        var saveData = {};
        var saveBuilds = {};
        for(var buildName in this.builds)
        {
            if(!this.builds.hasOwnProperty(buildName))
            {
                continue;
            }
            saveBuilds[buildName] = this.builds[buildName].saveSerialize();
        }
        return settings.set('allDolphinBuilds', saveBuilds)
            .catch((e)=>{
                throw e;
            });
    }

    syncToServer(){
        var buildData = {};
        for(var i in this.builds){
            if(!this.builds.hasOwnProperty(i))
            {
                continue;
            }
            var build = this.builds[i];
            buildData[build.id] = build.syncSerialize();
        }
        return SmashladderApi.post(constants.getUrl(constants.apiEndpoints.SYNC_BUILDS), {builds: JSON.stringify(buildData)})
            .then(response=>{
                return response.json();
            }).catch((error)=>{
                throw (error);
            });
    }

    static retrieveActiveBuilds() {
        let dolphinBuildsVersion = settings.getSync('dolphinBuildsVersion');
        if(dolphinBuildsVersion != 2)
        {
            console.log('resetting builds');
            settings.set('allDolphinBuilds', {});
            settings.set('dolphinBuildsVersion', 2);
            settings.set('ladderGames', {});
        }
        return settings.get('allDolphinBuilds')
            .then((builds) => {
                for (var i in builds) {
                    if (builds.hasOwnProperty(i)) {
                        var allBuilds = new Builds(builds);
                        Builds.fetchBuilds(allBuilds);//Get Updated list also
                        return allBuilds;
                    }
                }
                return Builds.fetchBuilds();
            }).catch((error) => {
                throw error;
            });
    };

    static fetchBuilds(previousAllBuilds) {
        return SmashladderApi.post(constants.getUrl(constants.apiEndpoints.DOLPHIN_BUILDS))
            .then((response) => {
                var contentType = response.headers.get("content-type");
                if (contentType && contentType.indexOf("application/json") !== -1) {
                    return response.json();
                }
                else {
                    response.text().then(text=> {
                        console.log(text)
                    });
                    throw new Error('Did not get a json response 2 ' );

                }
            }).then((json) => {
                console.log('[BUILDS]', json);
                if (json.builds) {
                    var hasBuilds = false;
                    var buildList = {};
                    var ladderList = new LadderMap();
                    for (var ladderId in json.builds) {
                        if (!json.builds.hasOwnProperty(ladderId)) {
                            continue;
                        }
                        hasBuilds = true;
                        let ladderGameData = json.builds[ladderId];
                        console.log(ladderGameData);
                        let ladderGame = LadderGame.updateGame(ladderGameData);
                        console.log(ladderGame);
                        ladderList.set(ladderId, ladderGame.serialize());
                        for(var buildIndex in ladderGameData.builds){
                            if(!ladderGameData.builds.hasOwnProperty(buildIndex))
                            {
                                continue;
                            }
                            let build = ladderGameData.builds[buildIndex];
                            if(!buildList[build.dolphin_build_id])
                            {
                                buildList[build.dolphin_build_id] = build;
                                buildList[build.dolphin_build_id].forLadders = {};
                            }
                            buildList[build.dolphin_build_id].forLadders[ladderId] = true;
                            buildList[build.dolphin_build_id] = build;
                        }
                    }
                    if (hasBuilds) {
                        if(previousAllBuilds)
                        {
                            previousAllBuilds.updateFromServer(buildList);
                            return previousAllBuilds;
                        }
                        else
                        {
                            return new Builds(buildList);
                        }
                    }
                    throw 'Json builds not found';
                }
                else {
                    console.log('error retrieving');
                }

            }).then((builds) => {
                builds.save();
                return builds;
            });
    }

    getBuildById(id){
        return this.builds[id];
    };

    static startGame(id){
        const build = Builds.instance.getBuildById(id);
        build.startGame();
    }

    static launchDolphinBuildById(id, data, closePrevious){
        const build = Builds.instance.getBuildById(id);


        build.host_code = data && data.parameters && data.parameters.host_code ? data.parameters.host_code : null;
        build.hostId = (data && data.data.host_id) ? data.data.host_id : null;

        var launchName = (data && data.data.game_launch_name) ? data.data.game_launch_name : null;

        if(!build)
        {
            Notification.showFocusedAlert('build not found...');
            return;
        }
        var launched;
        if(build.host_code)
        {
            launched = build.join(build.host_code, closePrevious);
        }
        else
        {
            if(launchName)
            {
                settings.set('lastLaunch', launchName);
            }
            launched = build.host(launchName, closePrevious);
        }
        return launched
            .catch(error=>{
                console.error(error);
            });
    }

    clearBuilds() {
        var builds = this.getBuilds();
        for(var i in builds)
        {
            if(!builds.hasOwnProperty(i))
            {
                continue;
            }
            builds[i].remove();
        }
        this.builds = {};

        return settings.set('allDolphinBuilds', {})
            .then( ()=>{

            }).catch((e)=>{
                throw e;
            });
    }
    
    getTemplateElementForBuild(build){
        var element = Builds.templateBuildElement.clone();
        element.attr('id', 'build_'+build.dolphin_build_id);
        element.appendTo(Builds.buildsContainer);
        return element;
    }
}

Builds.instance = null;
Builds.templateBuildElement = null;
Builds.buildsContainer = null;

module.exports = Builds;