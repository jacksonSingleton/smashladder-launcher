
const SmashladderApi = require('./SmashladderApi');
const constants = require('./constants');
const DolphinPlayer = require('./match/DolphinPlayer');

class DolphinActions
{
    static call(name, build, message) {
        if (DolphinActions.isCallable(name)) {
            return DolphinActions.callableActions[name](build, message);
        }
        else {
            throw 'Invalid Call to ' + name;
        }
    }

    static isCallable(name) {
        return typeof DolphinActions.callableActions[name] === "function";
    }

}

DolphinActions.callableActions = {
    host_code: function(build, value){
        if(build.ignoreNextHostMessage)
        {
            console.log('Ignoring host because attempting to join!');
            build.ignoreNextHost(false);
            return;
        }
        SmashladderApi.post(constants.getUrl(constants.apiEndpoints.DOLPHIN_HOST), {host_code: value} );
    },

    joining: function(build, value){
      build.ignoreNextHost(true);
    },

    player_list_info: function(build, value){
        if(false)
        {
            value = `Anther[1] : 727(0b00f1f) Win | 1------- |
                Ping: 0ms
                Status: ready
                0hpz[2] : 727(0b00f1f) Win | -2------- |
                Ping: 0ms
                Status: ready
                `;
        }
        return DolphinPlayer.parseDolphinPlayerList(value);
    },

    dolphin: function(value){
        console.log('Yay');
    }
};

module.exports = DolphinActions;