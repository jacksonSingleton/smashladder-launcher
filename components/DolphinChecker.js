const winprocess = require('winprocess');
const LadderMemoryProcessor = require('./LadderMemoryProcessor');
class DolphinChecker
{
    static dolphinIsRunning(){
        let pid = LadderMemoryProcessor.getDolphinPid();
        if(pid)
        {
            if(DolphinChecker.lastValidPid !== null && pid != DolphinChecker.lastValidPid)
            {
                console.log('Dolphin changed it up');
                DolphinChecker.lastValidPid = null;
                return false;
            }
            DolphinChecker.lastValidPid = pid;
            return pid;
        }
        return false;
    }

    static checkDolphinRunningLoop(){
        if(DolphinChecker.openRunning)
        {
            return;
        }
        DolphinChecker.openRunning = true;
        let intervalTimer = setInterval(()=>{
            if(DolphinChecker.dolphinIsRunning())
            {
                DolphinChecker.openRunning = false;
                DolphinChecker.pendingOpenPromises.forEach((promise)=>{
                    promise(DolphinChecker.lastValidPid);
                });
                DolphinChecker.pendingOpenPromises = [];
                clearInterval(intervalTimer);
                return true;
            }
        },4000);
    }

    static checkDolphinClosingLoop(){
        if(DolphinChecker.closeRunning)
        {
            return;
        }
        DolphinChecker.closeRunning = true;
        let intervalTimer = setInterval(()=>{
            if(!DolphinChecker.dolphinIsRunning())
            {
                DolphinChecker.closeRunning = false;
                DolphinChecker.pendingClosePromises.forEach((promise)=>{
                    promise();
                });
                DolphinChecker.pendingClosePromises = [];
                clearInterval(intervalTimer);
                return true;
            }
        },5000);
    }

    static waitForDolphinToOpen(){
        var fillmentReference;
        var promise = new Promise((fulfill,reject)=>{
            fillmentReference = fulfill;
        });
        DolphinChecker.checkDolphinRunningLoop();
        DolphinChecker.pendingOpenPromises.push(fillmentReference);
        return promise;
    }

    static waitForDolphinToClose(){
        var fillmentReference;
        var promise = new Promise((fulfill,reject)=>{
            fillmentReference = fulfill;
        });
        DolphinChecker.checkDolphinClosingLoop();
        DolphinChecker.pendingClosePromises.push(fillmentReference);
        return promise;
    }
}
DolphinChecker.openRunning = false;
DolphinChecker.closeRunning = false;

DolphinChecker.pendingOpenPromises = [];
DolphinChecker.pendingClosePromises = [];
DolphinChecker.lastValidPid = null;
module.exports = DolphinChecker;