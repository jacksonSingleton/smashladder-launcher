
var FormData = require('form-data');

class JsonHelper{

    static tryParse(string){
        try {
            return {data: JSON.parse(string), valid: true};

        } catch (e) {
            return {data: string, valid: false};
        }
    }

    static convertObjectToFormData(data){
        var sendData = new FormData();

        for(var key in data)
        {
            if(!data.hasOwnProperty(key))
            {
                continue;
            }
            sendData.append(key, data[key]);
        }
        return sendData;
    };
}

module.exports = JsonHelper;