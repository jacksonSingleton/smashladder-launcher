
const AppSettings = require('./AppSettings');
const LadderMap = require('./helpers/LadderMap');

class LadderGame
{
    constructor(data){
        if(!data.ladder || !data.builds)
        {
            throw new Error('Invalid ladder game data');
        }
        this.data = data;
        this.buildIds  = {};
        for(var i in this.data.builds){
            if(!this.data.builds.hasOwnProperty(i))
            {
                continue;
            }
            this.buildIds[this.data.builds[i].dolphin_build_id] = this.data.builds[i].dolphin_build_id;
        }
    }

    static retrieve(gameId){
        return LadderGame.games.get(gameId);
    }

    static updateGame(data, skipSave){
        if(LadderGame.games.get(data.ladder.id))
        {
            return LadderGame.games.get(data.ladder.id);
        }
        else
        {
            let game = new LadderGame(data);
            LadderGame.games.set(data.ladder.id, game);
            if(!skipSave)
            {
                let saveData = {};
                LadderGame.games.forEach((element)=>{
                   saveData[element.data.ladder.id] = element.serialize();
                });
                console.log('save attempt?', saveData);
                AppSettings.set('ladderGames', saveData);
            }
            return game;
        }

    }

    getHint(){
        return this.data.ladder.dolphin_game_id_hint;
    }

    getTitle(){
        return this.data.ladder.name;
    }

    static getGames(){
        return LadderGame.setupDefaults();
    }

    getPrimaryBuild(builds){
        console.log(this.buildIds);
        for(var i in this.buildIds){
            if(!this.buildIds.hasOwnProperty(i))
            {
                continue;
            }
            return builds.getBuildById(i);
        }
        return null;
    }

    serialize(){
        return this.data;
    }

    static setupDefaults(){
        if(LadderGame.hasSetupDefaults)
        {
            return LadderGame.games;
        }
        LadderGame.hasSetupDefaults = true;
        let ladderGames = AppSettings.getSync('ladderGames');
        let count = 0;
        for(var i in ladderGames)
        {
            if(!ladderGames.hasOwnProperty(i))
            {
                continue;
            }
            count++;
            let game = ladderGames[i];
            LadderGame.updateGame(game, true);
        }
        console.log('added ', count);
        if(!count)
        {
            LadderGame.hasSetupDefaults = false;
        }
        return LadderGame.games;
    }
}
LadderGame.games = new LadderMap();
LadderGame.hasSetupDefaults = false;


module.exports = LadderGame;