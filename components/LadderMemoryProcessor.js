const winprocess = require('winprocess');

class LadderMemoryProcessor
{
    constructor(){
        this.dolphin = null;
        this.pid = null;
        this.offset = 0x7FFF0000;
        this.calls = 0;
    }

    static getDolphinPid(){
        var pid = winprocess.getProcessId("Dolphin.exe");
        if(pid < 0)
        {
            return null;
        }
        return pid;
    }

    attach(){
        var pid = LadderMemoryProcessor.getDolphinPid();
        if(!pid)
        {
            throw new Error('no pid');
        }
        this.dolphin = new winprocess.Process(pid);
        this.pid = pid;
        this.dolphin.open();
        return Promise.resolve();
    }

    detach(){
        if(this.dolphin)
        {
            //Calling close crashes the process..
        }
        this.dolphin = null;
    }

    readProcessMemory(address, length){
        this.calls++;
        if(!this.dolphin)
        {
            Promise.reject(new Error('Dolphin Not Attached!'));
        }
        address = address + this.offset;
        var uint8Array = this.dolphin.readMemory(address, length);
        if(!(uint8Array instanceof Uint8Array))
        {
            return Promise.reject(new Error('Error reading memory'));
        }
        return Promise.resolve(new DataView(uint8Array.buffer));
    }
}

module.exports = LadderMemoryProcessor;