

const winprocess = require('winprocess');
const StatPlayer = require('./StatPlayer');
const MemoryAddresses = require('./MemoryAddresses');
const MemoryCache = require('./MemoryCache');
class MemoryTest 
{
    log(){
        console.log(arguments);
    }
    
    constructor(){
        this.offset = 0;
        this.process = null;
        this.memoryCache = new MemoryCache(this);

        this.players = new Map();
        this.activePlayers = new Map();
        for(var i = 1; i <= 4; i++){
            this.players.set(i,new StatPlayer(i, this));
            this.players.get(i).setActive(true);
        }

        var pid = winprocess.getProcessId("Dolphin.exe");
        if(pid < 0)
        {
            throw new Error('no pid');
        }
        console.log(pid);
        var notepad = new winprocess.Process(pid);
        console.log(notepad);
        if(notepad)
        {
            notepad.open();
            this.process = notepad;
            this.offset = 0x7FFF0000;
            // this.offset = 0x80000000;

            setTimeout(()=>{
                console.log('hello?');
                this.readMemory(MemoryAddresses.characterSelect.address, MemoryAddresses.characterSelect.length*4).then((view)=>{
                    /**
                     * 0x3F0E08
                     0x3F0E2C
                     0x3F0E50
                     0x3F0E74
                     */
                    for(var [playerIndex, player] of this.players)
                    {
                        let playerOffset = MemoryAddresses.characterSelect.length * player.addressIndex;
                        player.updateStat('playerType', view.getUint8(playerOffset));
                        player.updateStat('costume', view.getUint8(playerOffset + 1));
                        player.updateStat('cssCharacter', view.getUint8(playerOffset + 2));
                        player.updateStat('cssHover', view.getUint8(playerOffset + 3));
                    }
                });
                console.log('goodbye?');
            },2000)
        }
        setTimeout(function(){
            console.log('close');
            // notepad.close();
            console.log('closed');
        }, 1000);
    }

    readProcessMemory(address, length){
        address = address + this.offset;
        var uint8Array = this.process.readMemory(address, length);
        console.log(uint8Array);
        if(!(uint8Array instanceof Uint8Array))
        {
            return Promise.reject(new Error('Error reading memory'));
        }
        var buffer = new ArrayBuffer(uint8Array.length);
        var view = new DataView(uint8Array.buffer);
        return Promise.resolve(view);
    }

}

module.exports = new MemoryTest();