
characterInstances = {};
class SSCharacter {

    construct(characterId) {
        this.id = characterId;
        if(characters[characterId])
        {
            this.name = characters[characterId];
        }
        else
        {
            console.log('Invalid Character Code?');
            this.name = 'Unknown';
        }
    }

    static retrieve(id){
        if(characterInstances[id])
        {
            return characterInstances[id];
        }
        if(characters[id])
        {
            return characterInstances[id] = new SSCharacter(id);
        }
        else
        {

        }
    }
}

const characters = {
    0: "Captain Falcon",
    1: "Donkey Kong",
    2: "Fox",
    3: "Mr. Game and Watch",
    4: "Kirby",
    5: "Bowser",
    6: "Link",
    7: "Luigi",
    8: "Mario",
    9: "Marth",
    10: "Mewtwo",
    11: "Ness",
    12: "Peach",
    13: "Pikachu",
    14: "Ice Climbers",
    15: "Jigglypuff",
    16: "Samus",
    17: "Yoshi",
    18: "Zelda",
    19: "Shiek",
    20: "Falco",
    21: "Young Link",
    22: "Dr. Mario",
    23: "Roy",
    24: "Pichu",
    25: "Ganondorf",
    26: "Empty",
};

module.exports = SSCharacter;