
const child = require('child_process');

const SSCharacter = require('./SSCharacter');
const net = require('net');
const socket  = require("socket.io-client")
const EventEmitter = require('events');
var ShiningStatsRequest = require("./ShiningStatsRequest.js");
const MatchGame = require("./match/MatchGame");
const MatchData = require("./match/MatchData");
const settings = require("./AppSettings");
const DolphinChecker = require("./DolphinChecker");

const electron = require('electron');
const BrowserWindow = electron.BrowserWindow;


class ShiningStatsConnection extends EventEmitter
{
    constructor(){
        super();
        this.connected = false;
        this.settingsCache = null;
        this.statPuller = null;
        this.statServer = null;
    }

    setStatsContainer(statsContainer){
        this.statsContainer = statsContainer;
        this.statsComments = statsContainer.find('.status');
    }

    setStatusMessage(comment){
        if(this.statsComments)
        {
            this.statsComments.removeClass('error');
            this.statsComments.text(comment);
        }
        console.log(comment);
    }

    setRemoteProcess(mainProcess, ipc){
        this.mainProcess = mainProcess;
        this.ipc = ipc;
    }

    setErrorMessage(comment)
    {
        if(this.statsComments)
        {
            this.statsComments.addClass('error');
            this.statsComments.text(comment);
        }
        console.log(comment);
    }


    getSettings(){
        var setting = settings.getSync('shiningStats');
        if(!setting)
        {
            setting = {};
            setting.active = true;
            this.saveSettings(setting);
        }
        return setting;
    }

    saveSettings(updated){
        settings.set('shiningStats', updated);
    }

    disconnect(){
        if(this.connected)
        {
            this.client.disconnect();
        }
        this.mainProcess.closeStatPuller();
    }

    setConnected(changeTo){
        this.connected = changeTo;
        if(changeTo)
        {
            this.statsContainer.addClass('connected');
        }
        else
        {
            this.statsContainer.removeClass('connected');
        }
    }

    connect(){
        if(this.connected !== false)
        {
            console.log('already connected');
            return;
        }
        this.stopRequestLoopInterval();



        var setting = this.getSettings();
        if(!setting.active)
        {
            this.setStatusMessage('Dolphin memory reading is disabled');
            return;
        }
        if(this.waitingForDolphin)
        {
            return;
        }

        this.mainProcess.openStatPuller();
        this.waitingForDolphin = true;

        this.setStatusMessage('Waiting for the right moment to strike...');

        DolphinChecker.waitForDolphinToOpen().then(()=>{
            this.waitingForDolphin = false;
            this.connected = null;
            this.requests = [];
            this.requestLoopIntervalActive = false;

            this.matches = new Map();
            MatchGame.endCurrentMatch();

            this.setStatusMessage('Connecting to Dolphin...');

            var client = socket("http://localhost:5000", {reconnection: false, transports: ["websocket"]});

            client.on('connect', (response) =>{
                this.setStatusMessage('Watching!');
                this.setConnected(true);
            });

            var terribleError = (response) => {
                this.setStatusMessage('Something went terribly wrong');
                this.setConnected(false);
                console.log('error!', response);
                if(this.getSettings().active)
                {
                    setTimeout(()=>{
                        this.connect();
                    }, 5000);
                }
            };

            client.on('connect_error', terribleError);
            client.on('error', terribleError);

            client.on('disconnect', (response)=>{
                this.setConnected(false);
                if(this.getSettings().active)
                {
                    setTimeout(()=>{
                        this.connect();
                    }, 2000);
                }
                this.setStatusMessage('Connection Closed');
            });

            client.on('matchUpdate', (response)=>{
                MatchGame.manager(response);
            });

            this.client = client;
            this.requestLoopInterval = null;

            MatchGame.setConnection(this);

            ShiningStatsRequest.connection = this;
        });
    }

    retrievePlayerStat(stat, playerNumber){
        return ShiningStatsRequest.createRequest(stat,playerNumber);
    }

    checkActiveMatch(){
        if(!this.requestLoopIntervalActive)
        {
            console.log('LOOP INACTIVE');
            return;
        }

        var loadedValues ={
            matchId: null,
            stage: null,
            seconds: null
        };

        var retrieveMenu = ()=>{
            return this.retrievePlayerStat('menu').then((response)=>{
                MatchGame.menuHolder.findCache('.active_menu').text(response);
            })
        };
        var retrieveMatch = ()=>{
            return this.retrievePlayerStat('id').then((matchId)=>{
                if(matchId === "0")
                {
                    matchId = null;
                }
                if(!matchId)
                {
                    throw new Error('No Active Match');
                }
                return loadedValues.matchId = matchId;
            })
        };
        var retrieveStage = ()=>{
            return this.retrievePlayerStat('stage').then((stage)=>{
                if(typeof stage == 'undefined')
                {
                    stage = null;
                }
                if(stage === 'Unknown' || stage == 'undefined')
                {
                    stage = null;
                }
                if(!stage)
                {
                    throw new Error('No Active Stage');
                }
                return loadedValues.stage = stage;
            });
        };
        var retrieveSeconds = ()=>{
            return this.retrievePlayerStat('seconds').then((seconds)=>{
                if(seconds === 'undefined')
                {
                    seconds = null;
                }
                loadedValues.seconds = seconds;
                var data = new MatchData(loadedValues.matchId, loadedValues.stage, loadedValues.seconds);
                MatchGame.managerFromMatchData(data);
            });
        };

        // retrieveMatch()
        retrieveMenu()
            .then(retrieveStage)
            .then(retrieveSeconds)
            .catch((error)=>{
                throw error;
            });
    }

    requestsFinishedFunction(){
        return ()=>{
            this.checkActiveMatch();
        };
    }

    startRequestLoop(){
        ShiningStatsRequest.setClient(this.client);

        this.stopRequestLoopInterval();
        this.requestLoopIntervalActive = true;
        this.checkActiveMatch();
        if(this.requestsFinishedAction)
        {
            this.removeListener('requestsFinished', this.requestsFinishedAction);
        }
        this.requestsFinishedAction = this.requestsFinishedFunction();
        this.on('requestsFinished', this.requestsFinishedAction);
    }

    stopRequestLoopInterval() {
        this.requestLoopIntervalActive = false;
    }

    
}


module.exports = new ShiningStatsConnection();