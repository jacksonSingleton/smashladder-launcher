var EventEmitter = require("events");
class ShiningStatsRequest extends EventEmitter{
    construct(){
        throw new Error('Not used anymore!');
        this.timeout = null;
    }

    static setClient(client){
        ShiningStatsRequest.receiving = false;
        ShiningStatsRequest.client = client;
    }

    static setConnection(connection){
        ShiningStatsRequest.connection = connection;
    }

    static verifyChecker(){
        if(!ShiningStatsRequest.client)
        {
            throw Error('No Client!');
        }
        if(!ShiningStatsRequest.connection)
        {
            throw Error('No ShiningStatsConnection!');
        }
        if(!ShiningStatsRequest.receiving)
        {
            ShiningStatsRequest.receiving = true;
            ShiningStatsRequest.client.on('data', function(data){
                data = data.toString();
                // console.log(data);
                data = data.split('\0');
                data.pop(); //Remove end character
                // console.warn('RESPONSE!!!!', data);

                for(var i in data){
                    if(data.hasOwnProperty(i))
                    {
                        if(data[i] === "null")
                        {
                            data[i] = null;
                        }
                        ShiningStatsRequest.gotResponse(data[i]);
                        return;//We only expect one response at a time
                    }
                }
                ShiningStatsRequest.gotResponse(null);
            });
        }
    }
    static createRequest(field, player){

        ShiningStatsRequest.verifyChecker();
        var request = new ShiningStatsRequest();
        request.request = field;
        request.setField(field).setPlayer(player);

        ShiningStatsRequest.pendingRequests.push(request);
        ShiningStatsRequest.startExecutions();

        return new Promise((resolve,reject)=>{
            request.resolve = resolve;
            request.reject = reject;
        });
    }

    static startExecutions(){
        if(ShiningStatsRequest.activeRequest)
        {
            // console.log('A request is still pending');
            return;
        }
        ShiningStatsRequest.activeRequest = ShiningStatsRequest.pendingRequests.shift();
        if(ShiningStatsRequest.activeRequest)
        {
            if(ShiningStatsRequest.activeRequest.rejected)
            {
                console.log('pending request was already rejected');
                ShiningStatsRequest.clearActiveRequest();
                return ShiningStatsRequest.startExecutions();
            }
            ShiningStatsRequest.activeRequest.execute();
        }
        else
        {
            ShiningStatsRequest.emitFinished();
        }
    }

    static emitFinished(){
        if(ShiningStatsRequest.emittingFinished)
        {
            return;
        }
        var timeToWait;
        ShiningStatsRequest.emittingFinished = true;

        if(!ShiningStatsRequest.timeSinceLastEmit)
        {
            timeToWait = 0;
        }
        else
        {
            var timeSinceLast = (Date.now() - ShiningStatsRequest.timeSinceLastEmit.getTime()) / 1000;
            if(timeSinceLast < 1)
            {
                timeToWait = 4000;
            }
            else
            {
                timeToWait = 0;
            }
        }

        setTimeout(()=>{
            ShiningStatsRequest.emittingFinished = false;
            ShiningStatsRequest.timeSinceLastEmit = new Date();
            ShiningStatsRequest.connection.emit('requestsFinished');
        }, timeToWait);
    }

    static gotResponse(data){
        if(ShiningStatsRequest.activeRequest)
        {
            ShiningStatsRequest.activeRequest.response = data;
            console.log('[SS Response]', ShiningStatsRequest.activeRequest.request, ShiningStatsRequest.activeRequest.response);
            ShiningStatsRequest.activeRequest.resolve(ShiningStatsRequest.activeRequest.response);



            ShiningStatsRequest.clearActiveRequest();
            ShiningStatsRequest.startExecutions();
        }
        else
        {

        }
    }

    serialize(){
        return {
            request: this.request,
            response: this.response
        };
    }

    static clearActiveRequest(){
        if(ShiningStatsRequest.activeRequest)
        {
            clearTimeout(ShiningStatsRequest.activeRequest.timeout);
            ShiningStatsRequest.activeRequest = null;
        }
    }

    execute(){
        var request = '';
        if(this.player)
        {
            request =  "p"+this.player+"\0";
        }
        request += this.field+"\0";
        ShiningStatsRequest.client.write(request, 'utf8');

        this.timeout =  setTimeout(()=>{
            this.reject(new Error('Request Timed Out: '+this.request));
            this.rejected = true;
            if(this === ShiningStatsRequest.activeRequest)
            {
                ShiningStatsRequest.clearActiveRequest();
                ShiningStatsRequest.startExecutions();
            }
        }, 1000);
    }


    setField(field){
        this.field = field;
        return this;
    }
    setPlayer(player){
        this.player = player;
        return this;
    }
    setResponse(response){
        this.response = response;
        return this;
    }
}

ShiningStatsRequest.noneArePending = function(){
    return ShiningStatsRequest.pendingRequests.length === 0;
};
ShiningStatsRequest.pendingRequests = [];
ShiningStatsRequest.client = null;
ShiningStatsRequest.activeRequest = null;

module.exports = ShiningStatsRequest;