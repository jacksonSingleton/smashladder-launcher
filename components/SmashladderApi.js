
const constants = require('./constants');
const JsonHelper = require('./JsonHelper');
const fetch = require('node-fetch');
var Authentication = require("./Authentication.js");

class SmashladderApi
{
    static post(url, data, useAuthentication) {
        console.log('[POST]', url, data);
        return DebugPause(2000)
            .then(()=>{
                if(typeof useAuthentication == 'undefined' || useAuthentication)
                {
                    return Authentication.load();
                }
                return Promise.resolve();
            })
            .then((authentication)=>{

                if(typeof data == 'undefined')
                {
                    data = {};
                }
                if(authentication)
                {
                    data.session_key = authentication.session.token;
                    data.session_player_id = authentication.player.id;
                }

                for(var i in data)
                {
                    if(!data.hasOwnProperty(i))
                    {
                        continue;
                    }
                    if(data[i] === null || typeof data[i] === 'undefined')
                    {
                        delete data[i];
                    }
                }
                // console.log(data);

                var options = {
                    method: 'POST',
                    body: JsonHelper.convertObjectToFormData(data),
                    headers: {
                        cookie: 'betasurf=1',
                    }
                };

                return fetch(url, options );
            });
    }

}


function DebugPause(time) {
    if(!constants.debugUrls || true)
    {
        return Promise.resolve();
    }
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve();
        }, time);
    });
}

SmashladderApi.authentication = null;

module.exports = SmashladderApi;