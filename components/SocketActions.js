"use strict";

const Builds = require('./Builds');
const BuildLaunchSmartDolphin = require('./BuildLaunchSmartDolphin');
const EventEmitter = require('events');
const Authentication = require("./Authentication.js");


class SocketActions extends EventEmitter {

    constructor(){
        super();
        this.browserWindow = null;

        this.callableActions = {

            selectVersion: (message) => {
                this.browserWindow.webContents.send('highlightBuild', message.data.dolphin_version.name);
            },

            hostNetplay: (message) => {
                this.browserWindow.webContents.send('hostNetplay' , message)
            },

            startNetplay: (message) => {
                //This only happens if dolphin is not already launched...
                if (!message.data || !message.data.dolphin_version.id) {
                    throw 'Dolphin Data not included';
                }
                this.browserWindow.webContents.send('startNetplay' , message);
            },

            quitDolphin: () => {
                this.browserWindow.webContents.send('quitDolphin');
            },

            startGame: (message) => {
                this.browserWindow.webContents.send('startGame', message);
            },

            disableConnection: (message) => {
                Authentication.load()
                    .then((authentication)=> {
                        if (authentication.session.id == message.data.session_id) {
                            console.log('[I GET TO LIVE]');
                        }
                        else {
                            this.emit('disableConnection');
                        }
                    })
                    .catch(function (error) {
                        console.error(error);
                    });

            },

            requestAuthentication: () => {
                this.mainWindow.webContents.send('requestAuthentication');
                //After a minute or so, goes back to disable connection
            }
        }
    }

    call(name, message) {
        if (this.isCallable(name)) {
            this.callableActions[name](message);
        }
        else {
            throw 'Invalid Call to ' + name;
        }
    }

    isCallable(name) {
        return typeof this.callableActions[name] === "function";
    }

    setBrowserWindow(browserWindow) {
        BuildLaunchSmartDolphin.mainWindow = browserWindow;
        this.browserWindow = browserWindow;
    }

}

module.exports = new SocketActions();
