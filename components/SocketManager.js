const SocketActions = require("./SocketActions");
const SocketConnection = require("./SocketConnection");
const RetryTimer = require("./RetryTimer");
const EventEmitter = require("events");

class SocketManager extends EventEmitter
{
    constructor(){
        super();
        this.canBeCalledAgain = true;
        this.retryTimer = new RetryTimer(()=>{
            this.connect()
        }, 'CONNECT RETRY TIMER');
        this.canBeCalledAgainTimer = new RetryTimer(()=> {
            this.enableCalling()
        }, 'CAN BE CALLED AGAIN TIMER');

        this.socketConnection = null;
        this.client = null;
        this.mainWindow = null;

        this.canConnect = true;
        this.authentication = null;

        this.lastPing = new Date();


    }

    stopCheckPingInterval(){
        if(this.checkPingInterval)
        {
            clearInterval(this.checkPingInterval);
        }
    }

    resetCheckPingInterval(){
        this.stopCheckPingInterval();
        this.lastPing = new Date();
        this.checkPingInterval = setInterval(()=>{ this.checkPing() }, 5000);
    }
    checkPing(){
        if(!this.lastPing)
        {
            console.log('Ignoring check, never received a ping');
            return;
        }
        var tooLongTime = 90;

        if(this.getSecondsSinceLastPing() > tooLongTime)
        {
            //Reset the connection
            this.stopCheckPingInterval();
            this.disableConnection();
            this.connect(true);
            console.log('Need to reset the connection!');
        }
    }

    getSecondsSinceLastPing(){
        return (Date.now() - this.lastPing.getTime()) / 1000;
    }

    resetLastPing(){
        this.getSecondsSinceLastPing();
        this.lastPing = new Date();
    }

    killTimers(){
        this.stopCheckPingInterval();
        this.retryTimer.cancel();
        this.canBeCalledAgainTimer.end();
    }

    setAuthentication(authentication){
        this.authentication = authentication;
    }

    setMainWindow(window){
        this.mainWindow = window;
        SocketActions.setBrowserWindow(window);
    }

    enableCalling(){
        this.canBeCalledAgain = true;
    }

    disconnect(){
        console.log('DISCONNECTING?');
        this.disableConnection();
    }

    disableConnection(){
        if(this.websocketConnection)
        {
            this.websocketConnection.close();
        }
        if(this.client)
        {
            this.client.abort();
        }
        this.killTimers();
        this.canConnect = false;
        this.canBeCalledAgain = true;
    }

    disableConnectionFunction(){
        return () => {
            this.emit('disableConnection');

            this.disableConnection();
        }
    }

    connect(bypassBlocks){
        if(bypassBlocks)
        {
            this.canConnect = true;
        }
        if(!this.authentication)
        {
            throw 'Authentication Required!';
        }
        var canRun = this.initialize();
        if(this.socketConnection)
        {
            return;
        }
        if(!this.canConnect)
        {
            this.retryTimer.cancel();
            this.canBeCalledAgainTimer.end();
            return;
        }

        this.socketConnection = new SocketConnection(this.authentication);
        var websocket = this.websocket = this.socketConnection.websocket;

        websocket.on('connectFailed', (error) => {
            this.mainWindow.webContents.send('socketDisconnected');
            this.retryTimer.setTimer(10000);

            console.log('Connect Error: ' + error.toString());
            this.socketConnection = null;
        });

        if(this.disableConnectionStore)
        {
            SocketActions.removeListener('disableConnection', this.disableConnectionStore);
        }

        this.disableConnectionStore = this.disableConnectionFunction();
        SocketActions.on('disableConnection', this.disableConnectionStore );

        websocket.on('connect', (connection) => {
            this.websocketConnection = connection;

            this.resetCheckPingInterval();

            this.websocketConnection.on('error', (error) => {
                this.mainWindow.webContents.send('socketDisconnected');
                this.retryTimer.setTimer(10000);

                console.log("Connection Error: " + error.toString());
            });

            this.websocketConnection.on('message', (message)=> {
                if (message.type === 'utf8') {
                    console.log(message);
                    message = message.utf8Data;
                    try{
                        message = JSON.parse(message);
                        if(message.functionCall)
                        {
                            if(SocketActions.isCallable(message.functionCall))
                            {
                                SocketActions.call(message.functionCall,message);
                            }
                            else
                            {
                                console.error('[CONSOLE ACTION NOT FOUND]', message.functionCall);
                            }
                        }
                        this.lastPing = new Date();
                    }
                    catch(error)
                    {
                        console.error(error);
                    }
                }
            });

            this.websocketConnection.on('close', () => {
                console.log('Was closed for some reason');

                this.mainWindow.webContents.send('socketDisconnected');
                this.retryTimer.setTimer(10000);
                this.socketConnection = null;
            });


            this.mainWindow.webContents.send('socketConnected');
        });

        this.socketConnection.connect()
            .catch(function(error){
                throw error;
            });

    };

    initialize(){
        if(!this.canBeCalledAgainTimer.isActive())
        {
            this.canBeCalledAgainTimer.setTimer(50000);
        }
        if(!this.canBeCalledAgain)
        {
            console.log('Reconnection too soon');
            this.retryTimer.setTimer(10000);
            return false;
        }
        this.canBeCalledAgain = false;
        return true;
    }
}

module.exports =  new SocketManager();