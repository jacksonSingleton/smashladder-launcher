class StatPlayer{

    setActive(value){
        if(value === this.isActive)
        {
            return;
        }
        this.isActive = value;
        if(value)
        {
            // this.puller.log('setting active!', this.slot);
            this.processor.activePlayers.set(this.slot, this);
        }
        else
        {
            // this.processor.log('setting inactive!', this.slot);
            this.processor.activePlayers.delete(this.slot, this);
        }
    }

    constructor(slot, processor){
        this.slot = slot;
        this.processor = processor;
        this.addressIndex = slot - 1;
        this.setActive(false);

        this.stats = {
            stocks: null,
            suicides: null,
            kills: null,
            damage: null,
            action: null,
            x: null,
            y: null
        };

        this.playerTypes ={
            0: 'human',
            1: 'cpu',
            2: 'demo?',
            3: 'disabled',
        };
        this.statCallbacks = {
            character: (value)=>{
                if(value > 26)
                {
                    return null;
                }
                return value;
            },
            playerType: (value)=>{
                return this.playerTypes[value];
            }
        },
            this.statEquals = {
                specificKills:(one, two) =>{
                    if(!one || !two)
                    {
                        return false;
                    }
                    for(var i in one){
                        if(!one.hasOwnProperty(i))
                            continue;
                        if(two[i] !== one[i])
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
    }

    reset(){
        for(var i in this.stats)
        {
            if(!this.stats.hasOwnProperty(i))
            {
                continue;
            }
            this.stats[i] = null;
        }
    }

    getStat(name, value){
        return this.stats[name];
    }

    updateStat(name, value){
        if(this.statCallbacks[name])
        {
            value = this.statCallbacks[name](value);
        }
        if(this.statEquals[name])
        {
            if(this.statEquals[name](value, this.stats[name]))
            {
                return false;
            }
        }
        else
        {
            if(this.stats[name] === value)
            {
                return false;
            }
        }

        this.stats[name] = value;
        var output = {
            players: {}
        };
        output.players[this.slot] = {};
        output.players[this.slot][name] = value;
        if(this.isActive)
        {
            this.processor.hasChanges = true;
            this.processor.log('player change',this.slot,  name, value);
        }
        return true;
    }
}

module.exports = StatPlayer;