const MemoryCache = require('./MemoryCache');
const StatPlayer = require('./StatPlayer');

class StatProcessor
{
    constructor(puller){
        this.puller = puller;
        this.hasChanges = false;

        this.memoryCache = new MemoryCache(this);
        this.players = new Map();
        this.activePlayers = new Map();
        for(var i = 1; i <= 4; i++){
            this.players.set(i,new StatPlayer(i, this));
        }
    }

    setServer(server){
        this.server = server;
    }

    process(stat){
        if(stat.condition && stat.condition(this) === false)
        {
            return Promise.resolve();
        }
        return this.puller.memoryProcessor.readProcessMemory(stat.address, stat.length)
            .then((view)=>{
                return stat.process(this, view);
            });
    }

    log(){
        return;
        console.log(arguments);
    }

    matchIsNotActive(){
        return (this.memoryCache.get('menu') === 129);
    }

    cacheValue(name, value){
        return this.memoryCache.cacheValue(name, value);
    }

    clearActiveMatchData(){
        this.cacheValue('stage', null);
        this.cacheValue('timeRemaining', null);
    }

    emitIfChanges(){
        if(this.hasChanges)
        {
            this.hasChanges = false;
            this.emitAll();
        }
        else
        {
        }
    }

    emitAll(){
        var output = {};

        output.players = {};
        var playerCount = 0;
        if(this.matchIsNotActive())
        {
            this.clearActiveMatchData();
        }
        for(let [i, player] of this.activePlayers){
            if(this.matchIsNotActive())
            {
                // player.reset();
                // continue;
            }
            if(!player.isActive)
            {
                throw new Error('this should not happen');
            }
            if(player.stats.x === null)
            {
                player.setActive(false);// So that we can emit their final stock stats
            }
            else
            {
                playerCount++;
            }
            output.players[player.slot] = player.stats;
        }
        if(playerCount <= 1)
        {
            output.players = {};
            this.clearActiveMatchData();
        }
        output.game = this.memoryCache.allData();
        // console.log(output);
        this.server.emit('matchUpdate', output);
        this.hasChanges = false;
    }
}

module.exports = StatProcessor;