var MemoryAddresses = require('./MemoryAddresses');
var StatProcessor = require('./StatProcessor');
//https://docs.google.com/spreadsheets/d/1JX2w-r2fuvWuNgGb6D3Cs4wHQKLFegZe2jhbBuIhCG8/edit#gid=12

class StatPuller {

    constructor(memoryProcessor){
        if(!memoryProcessor)
        {
            throw new Error('Memory processor is necessary!');
        }

        this.statProcessor = new StatProcessor(this);
        this.memoryAttached = false;
        this.memoryProcessor = memoryProcessor;
        this.statProcessor.memoryProcessor = memoryProcessor;
    }

    setServer(server){
        this.server = server;
        this.statProcessor.setServer(server);
        this.server.on('connect', ()=>{
            this.log('CONNECTION');
            this.statProcessor.emitAll();
        })
    }

    


    start(){
        if(this.running)
        {
            return;
        }
        else if(this.memoryAttached === false)
        {
            console.log('attaching');
            this.memoryAttached = null;
            return this.memoryProcessor.attach()
                .then(()=>{
                    this.memoryAttached = true;
                    this.log('attached');
                    this.start();
                }).catch(()=>{
                    this.memoryAttached = false;
                    this.log('failed to attach');
                    setTimeout(()=>{
                        this.log('retry');
                        this.start();
                    },1000);
                });
        }
        else if(this.memoryAttached === null)
        {
            this.log('attempting attachment');
            return;
        }

        this.stop();
        this.running = true;
        this.loopNumber = 0;
        this.loopTimeLimit = 30;
        this.defaultLoopTimeLimit = 8;
        this.defaultLoopTimeLimit = 30;
        var loopRunner = ()=>{
            if(!this.running || ( false && this.promiseActive ))
            {
                //Process memory hangs so we just have to keep firing
                this.log('another loop was running');
                return;
            }
            this.log('current time limit ', this.loopTimeLimit);
            this.loopNumber++;
            this.promiseActive = true;
            this.log('waiting');
            this.all(this.loopTimeLimit).then(()=>{
                this.log('success');
                this.promiseActive = false;
                this.loopTimeLimit = this.defaultLoopTimeLimit;
                setTimeout(loopRunner, this.loopTimeLimit);
            }).catch((error)=>{
                this.log('failed', error);
                if(this.loopTimeLimit < 20000)
                {
                    this.loopTimeLimit += 200
                }
                this.promiseActive = false;
                setTimeout(loopRunner, this.loopTimeLimit);
            });
        };
        setTimeout(loopRunner, this.loopTimeLimit);
    }

    stop(){
        this.running = false;
    }

    log(){
        return;
        console.log(arguments);
    }

    all(){
        return new Promise((resolve,reject)=>{
            let timedOut = false;
            let timeout = setTimeout(()=>{
                timedOut = true;
                reject(new Error('Took too long to execute'));
            }, this.loopTimeLimit);
            this.timeRemaining()
                .then(()=>{
                    this.log(1, 'stage');
                    return this.stage()
                })
                .then(()=>{
                    this.log('playerStats');
                    return this.playerStats();
                })
                .then(()=>{
                    this.log('playerSpecific');
                    return this.playerSpecific()
                })
                .then(()=>{
                    this.log('characterSelect');
                    return this.characterSelect();
                })
                .then(()=>{
                    this.log('menu');
                    return this.menu()
                })
                .then(()=>{
                    clearTimeout(timeout);
                    if(timedOut)
                    {
                        return false;
                    }
                    this.statProcessor.emitIfChanges();
                    resolve();
                })
                .catch(error=>{
                    clearTimeout(timeout);
                    // console.trace(error);
                    if(timedOut)
                    {
                        return false;
                    }
                    this.statProcessor.emitIfChanges();
                    resolve();
                });
        })
    }

    readMemory(stat) {
        return this.statProcessor.process(stat);
    }

    timeRemaining(){
        return this.readMemory(MemoryAddresses.timeRemaining)
    }

    stage(){
        return this.readMemory(MemoryAddresses.stage);
    }


    menu(){
        return this.readMemory(MemoryAddresses.menu)
    }

    globalFrameCounter(){
        return this.readMemory(MemoryAddresses.globalFrameCounter)
    }

    characterSelect(){
        return this.readMemory(MemoryAddresses.characterSelect)
    }

    playerSpecific(){
        return this.readMemory(MemoryAddresses.matchPlayers)
    }

    playerStats(){
        return this.readMemory(MemoryAddresses.matchPlayerStats);
    }

}

module.exports = StatPuller;