
$ = require('jquery');
var fs = require('fs');

class ViewManager
{
    constructor(body, content){
        this.body = body;
        this.mainContentHolder = content;

        this.cachedViews = [];
        this.viewMode = null;
        this.currentView = null;

        this.activeXhr = null;
    }

    removeCurrentView(){
        if(this.currentView)
        {
            this.body.removeClass('view-'+this.currentView);
            this.cachedViews[this.currentView].detach();
            this.currentView = null;
        }
    }

    setCurrentView(viewName, content){
        this.removeCurrentView();

        this.cachedViews[viewName] = content;
        this.cachedViews[viewName].appendTo(this.mainContentHolder);

        this.currentView = viewName;
        this.body.addClass('view-'+viewName);
    }

    changeView(viewName){

        if(this.cachedViews[viewName])
        {
            this.setCurrentView(viewName, this.cachedViews[viewName]);
        }
        else
        {
            if(this.activeXhr && this.activeXhr.abort())
            {
                this.activeXhr.abort();
            }
            this.activeXhr = $.get('./views/'+viewName+'.html', (response) => {
                var content = $(response);

                this.setCurrentView(viewName, content)

                try{
                    require('../views/code/'+viewName+'.js');
                }
                catch(e)
                {
                    console.trace(e);
                    if(e.code !== 'MODULE_NOT_FOUND')
                    {
                        throw e;
                    }
                    console.log('not found '+viewName);
                }
            });
        }

    }


}

module.exports = ViewManager;