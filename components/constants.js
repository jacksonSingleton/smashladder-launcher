"use strict";

require ('hazardous');
const electron = require('electron');
var constants = {};

constants.root = require('app-root-path').toString();

constants.debugUrls = false;
constants.debugConsole = false;

var hosts = {
    dev: function(){
        constants.SITE_URL = 'http://localhost/smashladder';
        constants.WEBSOCKET_URL = 'ws://localhost:100';
        constants.APP_UPDATES_URL = 'http://localhost/smashladder/downloads/launcher/';
    },
    live: function(){
        constants.SITE_URL = 'https://www.smashladder.com';
        constants.WEBSOCKET_URL = 'wss://www.smashladder.com';
        constants.APP_UPDATES_URL = 'https://www.smashladder.com/downloads/launcher/';

    }
};

if(constants.debugUrls)
{
    hosts.dev();
}
else
{
    hosts.live();
}



constants.apiEndpoints = {
    SYNC_BUILDS:                '/apiv1/sync_builds',
    CLOSED_DOLPHIN:             '/apiv1/closed_dolphin_host',
    OPENED_DOLPHIN:             '/apiv1/opened_dolphin',
    DOLPHIN_BUILDS:             '/apiv1/all_dolphin_builds',
    UPDATE_BUILD_PREFERENCES:   '/apiv1/update_build_preference_active',
    DOLPHIN_HOST:               '/apiv1/dolphin_host',
    LOGOUT:                     '/logout',
    AUTHENTICATE_SESSION:       '/authenticate',
    DOLPHIN_LAUNCHER_LOGIN:     '/log-in/dolphin',
    REPORT_MATCH_GAME:          '/apiv1/report_match_game',
    RETRIEVE_MATCH_GAME_ID:     '/apiv1/retrieve_match_game_id',
};

constants.getUrl = function(path){
    return constants.SITE_URL + path;
};
constants.getPlayer = function(path){
    var settings = require('./AppSettings');
    return settings.getSync('userAuthentication').player;
};



module.exports = constants;
