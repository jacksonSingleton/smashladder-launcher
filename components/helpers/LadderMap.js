
class LadderMap extends Map
{

    toObject(){
        let data = {};
        for(let [key, value] of this)
        {
            data[key] = value;
        }
        return data;
    }
}
module.exports = LadderMap;