const $ = require('jquery');
$.fn.findCache = function(selector){
    var element = this;

    if(!this.data('findCache'))
    {
        this.data('findCache', {});
    }
    if(!this.data('findCache'))
    {
        console.error('UNDEFINED ELEMENT', this, selector);
        return $();
    }
    if(this.data('findCache')[selector])
    {
        return this.data('findCache')[selector];
    }
    else
    {
        var result = this.find(selector);
        this.data('findCache')[selector] = result;
        return result;
    }
};

$.fn.findCacheDisplayText = function(selector, value){
    var element = this;
    var other = this.findCache(selector);
    if(other.data('cacheDisplay') !== value){
        return other.data('cacheDisplay', value).text(value);
    }
    return other;
};
module.exports = $;