class DolphinPlayer
{
    /** SLOTS MAY CHANGE, so do not store the instance */
    constructor(name, slot){
        this.element = DolphinPlayer.retrieveElement(slot);

        if(!slot)
        {
            throw new Error('Invalid slot construct');
        }

        this.setUsername(name);
        this.slot = slot;
    }

    getAliasName(){
        if(this.getUsername())
        {
            return this.getUsername();
        }
        else
        {
            return '[-CPU-]';
        }
    }

    static setDolphinPlayerTemplate(dolphinPlayerTemplate){
        DolphinPlayer.dolphinPlayerTemplate = dolphinPlayerTemplate;
    }

    static retrieveElement(slot){
        if(DolphinPlayer.playerElements[slot])
        {
            return DolphinPlayer.playerElements[slot];
        }
        var element = DolphinPlayer.dolphinPlayerTemplate.clone().attr('id', 'dolphin_player_number_'+slot);
        element.appendTo('#dolphin_players');
        element.findCache('.player_number').text(slot);
        return DolphinPlayer.playerElements[slot] = element;
    }

    getUsername(){
        return this.name;
    }

    setUsername(username){
        this.name = username;
        // console.trace('Dolphin player', this.getAliasName(), this.slot);
        this.element.findCache('.username').text(this.getAliasName());
    }

    usernameIs(username){
        if(!this.name)
        {
            return false;
        }
        return this.name.toLowerCase() == username.toLowerCase();
    }

    static parseDolphinPlayerList(value){
        var valueSplit = value.split(/\r?\n/);

        // MatchGame.resetPlayerList();


        var changedPlayers = {};
        for(var i in valueSplit)
        {
            if(!valueSplit.hasOwnProperty(i))
                continue;
            var current = valueSplit[i];
            if(!current.includes("["))
            {
                continue;
            }

            var usernameSide = current.substring(0, current.lastIndexOf(":"));
            var systemInfoSide = current.substring(current.lastIndexOf(":")+1);

            var ports = systemInfoSide.substring(systemInfoSide.indexOf('|')+1, systemInfoSide.lastIndexOf('|')-1).trim();
            var systemInformation = systemInfoSide.substring(0, systemInfoSide.indexOf('|')).trim();

            var portIndexes = [];
            for (var characterIndex = 0; characterIndex < ports.length; characterIndex++) {
                if(ports.charAt(characterIndex) == '-')
                {
                    continue;
                }
                portIndexes.push(characterIndex+1);
            }
            let slot = null;
            if(portIndexes.length)
            {
                slot = portIndexes[0];
            }
            var username = usernameSide.substring(0,usernameSide.lastIndexOf("[")).trim();
            changedPlayers[slot] = DolphinPlayer.setPlayer(slot, username);
        }
        for(var [slot,nothing] of DolphinPlayer.possiblePlayers)
        {
            if(changedPlayers[slot])
            {
                continue;
            }
            DolphinPlayer.setPlayer(slot,null);
        }

        return changedPlayers;
    }

    static retrievePlayer(slot){
        if(!slot)
        {
            throw new Error('Invalid slot');
        }
        if(DolphinPlayer.playerElements[slot])
        {
            return DolphinPlayer.playerElements[slot];
        }
        else
        {
            return DolphinPlayer.playerElements[slot] = new DolphinPlayer(null, slot);
        }
    }

    static setPlayer(slot, name){
        var dolphinPlayer = DolphinPlayer.retrievePlayer(slot);
        dolphinPlayer.setUsername(name);
        return dolphinPlayer;
    }

}
DolphinPlayer.playerElements = {};
DolphinPlayer.possiblePlayers = new Map([
    [1, null],
    [2, null],
    [3, null],
    [4, null]
]);

module.exports = DolphinPlayer;