class LadderPlayer
{
    constructor(ladderPlayer){
        this.ladderPlayer = ladderPlayer;
    }

    getId(){
        return this.ladderPlayer.id;
    }

    getUsername(){
        return this.ladderPlayer.username;
    }
}

module.exports = LadderPlayer;