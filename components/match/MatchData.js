class MatchData{
    constructor(matchId, stage, timeRemaining){
        this.id = matchId;
        if(stage === '0')
        {
            stage = null;
        }
        this.stage = stage;
        this.timeRemaining = parseInt(timeRemaining);
        if(isNaN(this.timeRemaining))
        {
            this.timeRemaining = null;
        }
    }

    getTimeRemaining(){
        return this.timeRemaining;
    }

    getStage(){
        return this.stage;
    }
}

module.exports = MatchData;