const EventEmitter = require('events');
const MeleeStage = require('./MeleeStage');
const MatchPlayer = require('./MatchPlayer');
const MatchData = require('./MatchData');
const DolphinPlayer = require('./DolphinPlayer');
const LadderPlayer = require('./LadderPlayer');
const StringManipulator = require("../StringManipulator.js");

class MatchGame extends EventEmitter
{
    constructor(matchData)
    {
        super();
        this.id = matchData.id;
        if(!(matchData instanceof MatchData))
        {
            throw new Error('Invalid matchdata!');
        }

        this.dolphinPlayers = new Map();
        this.matchPlayers = new Map();
        this.ladderPlayers = new Map();
        this.retrievedLadderPlayers = false;


        this.startTime = new Date();
        this.endTime = null;
        this.ladderId = null;
        this.ladderMatch = null;

        this.timeStartedWith = null;
        this.timeRemaining = null;

        this.stocks = null;

        this.sequenceNumber = MatchGame.sequence++;

        this.element = MatchGame.matchTemplate.clone().appendTo(MatchGame.matchHolder);

        this.setTimeRemaining(matchData.getTimeRemaining());

        this.setStage(matchData.getStage());
        this.setElement('match_number', this.sequenceNumber);
        this.setElement('match_id', this.id);
        this.button = MatchGame.matchButtonTemplate.clone().val(this.sequenceNumber).appendTo(MatchGame.matchButtonHolder);
        this.button.data('matchElement', this.element);
        this.updateTitle();

        if(!MatchGame.removedNoMatches)
        {
            MatchGame.removedNoMatches = true;
            MatchGame.matchButtonHolder.find('.no_matches').remove();
        }

        MatchGame.matchButtonHolder.val(this.sequenceNumber).change();
        this.element.on('click', ()=>{
            console.log(this);
        });
        this.started = false;
        return this;
    }

    updateTitle(){
        this.button.text(this.getTitle());
    }

    getTitle(){
        if(!this.started)
        {
            return '#'+this.sequenceNumber;
        }
        let title = [];
        title.push('#' + this.sequenceNumber);
        title.push(this.getStage().name);
        for(let [i,matchPlayer] of this.matchPlayers){
            if(!matchPlayer.dolphinPlayer)
            {
                continue;
            }
            title.push(matchPlayer.dolphinPlayer.getAliasName() +' '+ '('+matchPlayer.stats.character.name+')');
        }
        let timeStringOptions = {hour: '2-digit', minute:'2-digit'};
        title.push(this.startTime.toLocaleTimeString([], timeStringOptions));
        if(this.endTime)
        {
            title.push('-');
            title.push(this.endTime.toLocaleTimeString([], timeStringOptions));
        }
        return title.join(' ');
    }

    static log(arg1, arg2)
    {
        return;
        console.log('[MATCHGAME]',arg1, arg2);
    }

    setElement(name, value){
        var element = this.element.findCache('.'+name);
        if(element.data('value') === value)
        {
            return;
        }
        element.data('value', value).text(value);
    }

    static setConnection(connection){
        MatchGame.connection = connection;
        return this;
    }

    setLadderId(id)
    {
        this.ladderId = id;
        this.setElement('ladder_id', this.ladderId);

        return this;
    }
    setMatchGameId(id)
    {
        this.matchGameId = id;
        this.setElement('match_game_id', this.matchGameId);

        return this;
    }

    setTimeRemaining(seconds){
        this.timeRemaining = parseInt(seconds);
        if(!this.timeStartedWith)
        {
            this.timeStartedWith = Math.ceil(this.timeRemaining / 60) * 60;
        }
        let timeEnd = new Date();
        this.setElement('match_seconds',
            StringManipulator.timeFormat(this.timeRemaining) +'/'+StringManipulator.timeFormat(this.timeStartedWith) );
    }

    getTimeRemaining(){
        return this.timeRemaining;
    }

    assignLadderPlayer(player){
        if(!(player instanceof LadderPlayer))
        {
            throw Error('Player is not an instance of player!');
        }
        for(let [slot, matchPlayer] of this.matchPlayers){
            if(!matchPlayer.dolphinPlayer)
            {
                continue;
            }
            if(matchPlayer.dolphinPlayer.usernameIs(player.getUsername())){
                matchPlayer.setLadderPlayer(player);
            }
        }
        return player;
    }

    setStage(stage){
        this.stage = MeleeStage.retrieve(stage);
        this.element.findCache('.match_stage').html(this.stage.img);
        return this;
    }

    getStage(){
        return this.stage;
    }
    
    serializeForLadder(){
        var serialized = {};
        serialized.players = {};
        for(let [slot, matchPlayer] of this.matchPlayers)
        {
            if(matchPlayer.stats.playerType != 'human')
            {
                throw new Error('Dolphin Player Slot '+ matchPlayer.dolphinPlayer.slot +' is cpu');
            }
            if(!matchPlayer.ladderPlayer)
            {
                throw new Error('Dolphin Username Slot '+ matchPlayer.dolphinPlayer.slot +' does not match a smashladder account');
            }
            serialized.players[matchPlayer.ladderPlayer.getId()] ={
                id: matchPlayer.ladderPlayer.getId(),
                character_name: matchPlayer.stats.character.name,
                costume: matchPlayer.stats.costume,
                stocks: matchPlayer.stats.stocks,
                // apm: matchPlayer.apm
            };
            for(let [stockNumber, stock] of matchPlayer.detailedStocks)
            {
                if(!serialized.players[matchPlayer.ladderPlayer.getId()].stock_detail)
                {
                    serialized.players[matchPlayer.ladderPlayer.getId()].stock_detail = {};
                }
                serialized.players[matchPlayer.ladderPlayer.getId()].stock_detail[stock.stockNumber]
                    = stock.serializeStock();
            }
        }
        if(!this.ladderMatch)
        {
            throw new Error('No match id was ever retrieved');
        }
        serialized.match_id = this.ladderMatch.id;
        if(!this.ladderMatch.game)
        {
            throw new Error("Ladder match does not have a game ready!");
        }

        var endTime = this.endTime ? this.endTime.getTime() : null;
        serialized.game = {
            id: this.ladderMatch.game.id,
            stage_name: this.getStage().name,
            time_started_with: this.timeStartedWith,
            time_remaining: this.getTimeRemaining(),
            start_time: this.startTime.getTime(),
            end_time: endTime,
        };

        return serialized;
    }

    reportToLadder(){
        try{
            var serialized = this.serializeForLadder();
        }
        catch(error){
            this.element.findCache('error').text(error);
            throw error;
        }
        var data = {
            match_report: JSON.stringify(serialized)
        };

        return MatchGame.postRetrieveReportMatchGame(data);
    }

    static postRetrieveReportMatchGame(data){
        return MatchGame.postRetrieveJson(MatchGame.constants.getUrl(MatchGame.constants.apiEndpoints.REPORT_MATCH_GAME), data).then((response)=>{
            this.matchHolder.addClass('sending');
            if(response.error)
            {

            }
        }).catch((error)=>{
            if(error)
            {
                throw error;
            }
            else
            {
                throw new Error('Error reporting match!');
            }
        });
    }

    getMatchPlayer(slotNumber){
        var player = this.matchPlayers.get(slotNumber);
        return player ? player : null;
    }

    start(){
        if(this.started)
        {
            return;
        }
        this.emit('matchStarted');
        this.retrieveMatchGameId();
        this.started = true;
        this.updateTitle();

        // this.retrieveMatchPlayers();
    }

    finish(){
        if(MatchGame.activeMatch === this)
        {
            MatchGame.previousMatch = this;
            MatchGame.activeMatch = null;
        }
        if(this.endTime)
        {
            return;
        }

        this.endTime = new Date();
        this.updateTitle();

        this.element.addClass('ended');
        this.button.addClass('ended');
        this.emit('matchFinished');

        if(this.isIncomplete())
        {
            this.button.addClass('incomplete');
            this.button.addClass('disabled');
            this.element.addClass('disabled');
        }
        else
        {
            this.reportToLadder();
        }
    }

    isIncomplete(){
        if(!this.matchPlayers.size)
        {
            return false;
        }
        for(let [slot, matchPlayer] of this.matchPlayers){
            if(!matchPlayer.stats.character)
            {
                return true;
            }
        }
    }


    retrieveMatchGameId(){
        if(this.retrievingMatchGameId)
        {
            return;
        }
        this.retrievingMatchGameId = true;
        MatchGame.postRetrieveMatchGameId()
            .then(response=>{
                MatchGame.log('LADDER ID ', response);
                if(response.error)
                {
                    this.emit('retrievedLadderDataError', response);
                    this.element.addClass('ladder_error');
                    this.element.findCache('.error_message').text(response.error);
                    return;
                }
                else
                {
                    this.element.removeClass('ladder_error');
                }
                if(response.match && response.match.id)
                {
                    this.ladderMatch = response.match;
                    this.setLadderId(response.match.id);
                    if(response.match.game)
                    {
                        this.setMatchGameId(response.match.game.id);
                    }
                    for(var i in response.match.players){
                        if(!response.match.players.hasOwnProperty(i))
                        {
                            continue;
                        }
                        var player = response.match.players[i];
                        this.assignLadderPlayer(new LadderPlayer(player.player));
                    }
                }
                this.emit('retrievedLadderData', response);


            }).catch((error)=>{
                throw (error);
            });
    }

    static postRetrieveMatchGameId(){
        return MatchGame.postRetrieveJson(MatchGame.constants.getUrl(MatchGame.constants.apiEndpoints.RETRIEVE_MATCH_GAME_ID, null))
            .then(response=>{
                var contentType = response.headers.get("content-type");
                if (contentType && contentType.indexOf("application/json") !== -1) {
                    try{
                        return response.json();

                    }catch(error){
                        MatchGame.log(response.text());
                    }
                }
                else {
                    MatchGame.log(response.text());
                    throw 'Did not get a json response 1';
                }
            });
    }

    static postRetrieveJson(url, data){
        return MatchGame.smashladderApi.post(url, data);
    }
    static setLadderApi(api){
        MatchGame.smashladderApi = api;
    }
    static setConstants(constants){
        MatchGame.constants = constants;
    }
    static setMatchTemplate(matchTemplate){
        MatchGame.matchTemplate = matchTemplate
    }
    static setMatchButtonTemplate(buttonTemplate){
        MatchGame.matchButtonTemplate = buttonTemplate
    }
    static setPlayerTemplate(playerTemplate){
        MatchPlayer.setPlayerTemplate(playerTemplate);
    }
    static setMatchHolder(matchHolder){
        MatchGame.matchHolder = matchHolder;
    }
    static setMatchButtonHolder(buttons){
        MatchGame.matchButtonHolder = buttons
    }
    static setJquery(jquery){
        MatchGame.$ = jquery;
    }
    static setMenuHolder(menuTemplate){
        MatchGame.menuHolder = menuTemplate;
    }
    static setDolphinPlayerTemplate(template){
        DolphinPlayer.setDolphinPlayerTemplate(template);
    }

    static setDebugElement(element){
        MatchGame.debugElement = element;
    }
    static setStatsContainer(element){
        MatchGame.statsContainer = element;
    }

    static activeMatchIsSame(matchData){ //TODO: Request a better way through the API to know this. The api can identify a sequence number for games.
        if(!MatchGame.activeMatch)
        {
            return false;
        }
        if(MatchGame.activeMatch.id)
        {
            MatchGame.log('id is diff');
            if(matchData.id != MatchGame.activeMatch.id)
            {
                return false
            }
        }
        if(MatchGame.activeMatch.getTimeRemaining() < matchData.getTimeRemaining())
        {
            MatchGame.log('time remaining mismatch');
            return false;
        }
        if(MatchGame.activeMatch.stage.id != matchData.stage)
        {
            MatchGame.log('stage difference');
            return false;
        }
        return true;
    }

    static setupNewMatch(matchData){
        if(!(matchData instanceof MatchData))
        {
            throw new Error('Match data not correct');
        }
        if(isNaN(matchData.getTimeRemaining()))
        {
            throw new Error('seconds are a required parameter');
        }
        var match =  new MatchGame(matchData);

        MatchGame.previousMatch = MatchGame.activeMatch;
        MatchGame.activeMatch = match;
        MatchGame.matches.set(match.getSessionId(), match);

        return match;
    }

    getSessionId(){
        return this.id + '|' + this.sequenceNumber;
    }

    static endCurrentMatch(){
        if(MatchGame.activeMatch)
        {
            MatchGame.activeMatch.finish();
            // console.error('acting match ended');
            MatchGame.activeMatch = null;
        }
    }

    static manager(matchResponse){
        MatchGame.log('Stats Data', matchResponse);
        let menu = null;
        let playingGame = null;
        if(MatchGame.activeMatch)
        {
        }
        else
        {
        }
        let startActiveMatch = MatchGame.activeMatch;
        if(matchResponse.game)
        {
            if(matchResponse.game.menu)
            {
                let menu = matchResponse.game.menu;
                if(matchResponse.game.stage)
                {
                    playingGame = true;
                    MatchGame.menuHolder.findCacheDisplayText('.active_menu', 'Playing Game');
                }
                else if(menu === 129)
                {
                    playingGame = false;
                    MatchGame.menuHolder.findCacheDisplayText('.active_menu', 'Menu');
                }
                else if(menu === 128)
                {
                    playingGame = false;
                    MatchGame.menuHolder.findCacheDisplayText('.active_menu', 'Stage Select');
                }
            }
            var data = new MatchData(matchResponse.game.id, matchResponse.game.stage, matchResponse.game.timeRemaining);
            try{
                MatchGame.managerFromMatchData(data);
            }catch(error){
                if(startActiveMatch)
                {
                    startActiveMatch.element.findCacheDisplayText('.errors', error);
                }
                MatchGame.log('had error but it is ok', error);
                //This is ok
            }
        }
        let currentMatch = MatchGame.activeMatch;
        if(!playingGame && MatchGame.previousMatch)
        {
            MatchGame.log('USE PREVIOUS MATCH');
            currentMatch = MatchGame.previousMatch;
        }
        if(matchResponse.players && currentMatch)
        {
            for(let slotNumber in matchResponse.players)
            {
                if(!matchResponse.players.hasOwnProperty(slotNumber))
                {
                    continue;
                }
                slotNumber = parseInt(slotNumber);
                let playerData = matchResponse.players[slotNumber];


                if(Number.isInteger(playerData.character))
                {
                    currentMatch.dolphinPlayers.set(slotNumber, DolphinPlayer.retrievePlayer(slotNumber));
                    var matchPlayer;
                    matchPlayer = currentMatch.matchPlayers.get(slotNumber);
                    if(!matchPlayer)
                    {
                        matchPlayer = new MatchPlayer(currentMatch, slotNumber);
                        currentMatch.matchPlayers.set(slotNumber, matchPlayer);
                    }

                    matchPlayer.updateValue('character', playerData.character);
                    matchPlayer.setDolphinPlayer(currentMatch.dolphinPlayers.get(slotNumber));
                }
                else
                {
                    throw new Error('Somehow this player does not have a character');
                }
                MatchGame.log('update ', matchPlayer);
                for(let name in playerData)
                {
                    if(!playerData.hasOwnProperty(name))
                    {
                        continue;
                    }
                    let value = playerData[name];
                    matchPlayer.updateValue(name, value);
                }
            }
        }
        if(MatchGame.activeMatch)
        {
            MatchGame.activeMatch.start();
        }

        return MatchGame.activeMatch;
    }
    /**
     *
     * @param matchData
     * @returns MatchGame | null - MatchGame on new match, null if a match ended
     */
    static managerFromMatchData(matchData){
        if(!(matchData instanceof MatchData))
        {
            throw new Error('Match data is required parameter!');
        }
        if(Number.isInteger(matchData.getTimeRemaining()) )
        {
            if(MatchGame.activeMatch)
            {
                if(MatchGame.activeMatchIsSame(matchData))
                {
                    MatchGame.activeMatch.setTimeRemaining(matchData.timeRemaining);
                }
                else
                {
                    MatchGame.endCurrentMatch();
                }
            }
            else if(matchData.getTimeRemaining())
            {
                // if((MatchGame.previousMatch && MatchGame.previousMatch.getTimeRemaining() != matchData.getTimeRemaining())
                {
                    return MatchGame.setupNewMatch(matchData);
                }
            }
            else
            {
            }

        }
        else
        {
            MatchGame.endCurrentMatch();
        }
        return null;
    }
}
MatchGame.sequence = 1;
MatchGame.activeMatch = null;
MatchGame.matches = new Map();

module.exports = MatchGame;