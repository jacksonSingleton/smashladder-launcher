const MeleeCharacter = require('./MeleeCharacter');
const PlayerStock = require('./PlayerStock');

class MatchPlayer /** Ties all the player elements together **/
{
    constructor(match, slot){
        this.ladderPlayer = null;
        this.dolphinPlayer = null;

        this.slot = slot;
        // if(!(match instanceof MatchGame))
        // {
        //     throw Error('Incorrect match value');
        // }
        this.match = match;
        this.currentStock = null;
        this.createdStockIcons = false;

        this.element = MatchPlayer.playerTemplate.clone();
        this.element.appendTo(match.element.findCache('.players'));
        this.element.findCache('.match_player_number').text(slot);

        this.element.on('click', ()=>{
            console.log(this);
        });

        this.detailedStocks = new Map();

        this.stats = {
            stocks: null,
            suicides: null,
            kills: null,
            damage: null
        };

        this.statEquals = {
            specificKills:(one, two) =>{
                if(!one || !two)
                {
                    return false;
                }
                for(var i in one){
                    if(!one.hasOwnProperty(i))
                        continue;
                    if(two[i] !== one[i])
                    {
                        return false;
                    }
                }
                return true;
            }
        },
            this.statCallbacks = {
                damage: (damage)=>{
                    this.setElement('damage', damage);
                    damage = parseInt(damage);
                    if(this.stats.damage !== null)
                    {
                        if(this.stats.damage > damage)
                        {
                            this.match.finish();
                            return;
                        }
                    }
                    return this.stats.damage = damage;
                },
                stocks: (stocks)=>{
                    if(!this.createdStockIcons && this.stats.costume)
                    {
                        this.createdStockIcons = true;
                        let matchStocksHolder = this.element.findCache('.match_stocks');
                        for(let i = 1; i <= stocks; i++)
                        {
                            let playerStock = new PlayerStock(this, i, this.stats.character.stockIcon(this.stats.costume));
                            this.detailedStocks.set(i, playerStock);
                            playerStock.element.appendTo(matchStocksHolder);
                        }
                        this.currentStock = this.detailedStocks.get(stocks);
                        if(this.currentStock)
                        {
                            this.currentStock.startStock();
                        }
                    }
                    if(this.stats['stocks'] === null)
                    {

                    }
                    else
                    {
                        if(stocks > this.stats['stocks']) //Match was probably reset so this match needs to be ended immediately
                        {
                            this.match.finish();
                            return;
                        }
                        if(stocks < this.stats['stocks'])
                        {
                            if(this.createdStockIcons)
                            {
                                this.currentStock = this.detailedStocks.get(stocks)
                                if(this.currentStock)
                                {
                                    this.currentStock.startStock();
                                }
                                let lastStock = this.detailedStocks.get(stocks + 1);
                                if(lastStock)
                                {
                                    this.detailedStocks.get(stocks + 1)
                                        .updateDamage(this.stats.damageReceived)
                                        .endStock();
                                }
                            }
                        }
                    }
                    this.stats['stocks'] = parseInt(stocks);
                    this.updateStocks();
                    return this.stats['stocks'];
                },
                suicides: (value)=>{
                    this.setElement('suicides', value);
                    return this.stats['suicides'] = parseInt(value);
                },
                kills: (value)=>{
                    this.setElement('kills', value);
                    return this.stats['kills']= parseInt(value);
                },
                specificKills: (value)=>{
                    setTimeout(()=>{
                        var images = [];
                        if(this.statEquals.specificKills(value,this.specificKills))
                        {
                            return this.specificKills;
                        }
                        for(var playerSlot in value){
                            var player = this.match.matchPlayers.get(parseInt(playerSlot));
                            if(player && player.stats.character)
                            {
                                let kills = value[playerSlot];
                                for(let i = 0; i < kills; i++)
                                {
                                    images.push(player.stats.character.stockIcon(player.stats.costume));
                                }
                            }
                            else
                            {
                            }
                        }
                        if(images.length)
                        {
                            this.element.findCache('.match_specific_kills').html(images.join(''));
                        }
                        else
                        {
                            this.element.findCache('.match_specific_kills').html('None');
                        }
                        this.specificKills = value;
                    }, 50);
                },
                character: (value)=>{
                    let character = MeleeCharacter.retrieve(value);
                    if(this.stats.character === character){
                        return this.stats.character;
                    }
                    if(character)
                    {
                        this.element.findCache('.match_character').html(character.img);
                    }
                    else
                    {
                        this.setElement('character', 'None');
                    }
                    return this.stats.character = character;
                },
                cssCharacter: (value)=>{
                    var character = MeleeCharacter.retrieveCss(value);
                    return this.stats['cssCharacter'] = character;
                },
                costume: (value)=>{
                    this.stats.costume = value;
                    this.updateStocks();
                    return this.stats.costume;
                }
            }
    }
    updateValue(name, value){
        if(this.statCallbacks[name])
        {
            this.stats[name] = this.statCallbacks[name](value);
        }
        else
        {
            this.stats[name] = value;
        }
    }

    static setPlayerTemplate(template){
        this.playerTemplate = template;
    }

    updateStocks(){
        let stockCount = this.stats.stocks;
        let name = 'stocks'
        if(this.stats.character && Number.isInteger(stockCount)  && Number.isInteger(this.stats.costume) )
        {
            this.element.findCache('.match_'+name).addClass('has_stock_icons');
        }
        else
        {
            this.element.findCache('.match_'+name).removeClass('addClass');
        }
    }

    setLadderPlayer(ladderPlayer){
        this.ladderPlayer = ladderPlayer;
    }

    setDolphinPlayer(dolphinPlayer){
        this.dolphinPlayer = dolphinPlayer;
        this.element.findCacheDisplayText('.player_name', dolphinPlayer.getAliasName());
    }

    setElement(name, value){
        this.element.findCacheDisplayText('.match_'+name, value);
    }

}

module.exports = MatchPlayer;