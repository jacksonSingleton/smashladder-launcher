const StringManipulator = require('../StringManipulator')

class PlayerStock
{
    constructor(matchPlayer, stockNumber, imageElement)
    {
        this.matchPlayer = matchPlayer;
        this.stockNumber = stockNumber;
        this.startTime = null;
        this.endTime = null;
        this.element = $(imageElement);
        this.damage = 0;
    }

    serializeStock(){
        let data = {};
        data.percent = this.damage;
        data.started = this.started;
        data.ended = this.ended;
        return data;
    }

    startStock(){
        this.started = this.matchPlayer.match.timeRemaining;
        this.damageAtBeginningOfStock =
            this.matchPlayer.stats.damageReceived ? this.matchPlayer.stats.damageReceived : 0;
        this.updateTooltip();
        return this;
    }

    updateTooltip(){
        if(this.ended)
        {
            let sentence = 'Ended at '
                + StringManipulator.timeFormat(this.ended) +' with '+this.damage+'%';
            return this.element.attr('data-tooltip', sentence).tooltip();
        }
        else
        {
            return this.element.tooltip('remove');
        }
    }

    endStock(){
        this.ended = this.matchPlayer.match.timeRemaining;
        this.element.addClass('dead');
        this.updateTooltip();
        return this;
    }

    updateDamage(damage){
        this.damage = damage - this.damageAtBeginningOfStock;
        return this;
    }
}

module.exports = PlayerStock;