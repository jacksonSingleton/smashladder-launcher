"use strict";

const WebSocket =  require('websocket').client;
const constants =  require('./constants');

var SocketConnection = function(authentication){
    this.authentication = authentication;
    this.websocket = new WebSocket();
    this.allowConnections = true;

};
SocketConnection.prototype.connect = function(){
    if(!this.allowConnections)
    {
        return Promise.reject();
    }
    var authentication = this.authentication;
    var connectData = {
        session_key: authentication.session.token,
        player_id: authentication.player.id,
        version: '1.0.0',
        type:5
    };
    return authentication.checkAuthentication()
        .then( () => {
            console.log('[CONNECTING TO SOCKET SERVER]');
            this.websocket.connect(constants.WEBSOCKET_URL+'?'+urlSerialize(connectData), '');
        }).catch(function(error){
            console.error(error);
            throw error;
        });

};

SocketConnection.prototype.disconnect = function(){
    throw 'hm';
    this.allowConnections = false;
};

module.exports = SocketConnection;

var urlSerialize = function(obj) {
    var str = [];
    for(var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
};
