"use strict";



const electron = require('electron');
const app = electron.app;
const Tray = electron.Tray;
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
const constants = require('./components/constants');

const Files = require("./components/Files.js");

const SocketManager = require('./components/SocketManager');
var Authentication = require("./components/Authentication.js");
var Settings = require("./components/AppSettings.js");
require("./components/AutoUpdater");
const DolphinChecker = require("./components/DolphinChecker.js");

var info = require('./package.json');

var mainWindow = null;
let tray = null;

const squirrelEvent = require('./components/Squirrel');

if (require('electron-squirrel-startup')) return;

// this should be placed at top of main.js to handle setup events quickly
if (squirrelEvent(app)) {
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
}

const shouldQuit = app.makeSingleInstance((commandLine, workingDirectory) => {
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
        if (mainWindow.isMinimized()) mainWindow.restore();
        mainWindow.focus();
    }
});

if (shouldQuit) {
    app.quit();
    return;
}



function initializeTray(){
    var loginSettings = getLoginItemSettings();
    console.log('autoUpdates', !!Settings.getSync('autoUpdates'));
    let quickHost = {
        label: "Host",
        submenu: [

        ]
    };
    let quickJoin = {
        label: "Join",
        submenu: [

        ]
    };
    const LadderGame = require('./components/LadderGame');
    LadderGame.getGames().forEach((game)=>{
        quickHost.submenu.push({
            label: game.getTitle(),
            click: ()=>{
                mainWindow.webContents.send('hostGame', game.serialize());
            }
       });
        quickJoin.submenu.push({
            label: game.getTitle(),
            click: ()=>{
                mainWindow.show();
                mainWindow.webContents.send('joinGame', game.serialize());
            }
       });
    });
    return [
        {
            label: 'Open Smashladder Dolphin Launcher '+info.version,
            click:  function(){
                mainWindow.show();
            }
        },
        {
            label: null,
            type: 'separator'
        },
        quickHost,
        quickJoin,
        {
            label: 'Random Match',
            enabled: false,
        },
        {
            label: null,
            type: 'separator'
        },
        {
            label: 'Launch At Startup',
            type: 'checkbox',
            checked: loginSettings.openAtLogin,
            click:  function(){
                changeLaunchAtStartup(!getLoginItemSettings().openAtLogin);
                updateTray();
            }
        },
        {
            label: 'Enable Auto Updates',
            type: 'checkbox',
            checked: true,
            enabled: false,
            click:  function(){
                console.log('writing', !Settings.getSync('autoUpdates'));
                Settings.set('autoUpdates', !Settings.getSync('autoUpdates'));
                updateTray();
            }
        },
        {
            label: null,
            type: 'separator'
        },
        {
            label: 'Quit',
            click:  function(){
                app.isQuiting = true;
                app.quit();
            }
        }
    ]
}

function updateTray()
{
    var items = initializeTray();
    const contextMenu = Menu.buildFromTemplate(initializeTray());
    tray.setContextMenu(contextMenu);
}

app.setAppUserModelId('com.squirrel.smashladderlauncher.SmashladderLauncher');
// Settings.set('startLaunchInitialized', false);
app.on('ready', function () {
    var startupLaunchInitialized = Settings.getSync('startLaunchInitialized');
    var startMinimized = (process.argv || []).indexOf('--hidden') !== -1;

    if(!startupLaunchInitialized)
    {
        changeLaunchAtStartup(true);
        Settings.set('startLaunchInitialized', true);
    }
    // const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(null);

    app.on('window-all-closed', function() {
        app.quit();
    });

    tray = new Tray(constants.root+'/images/android-icon-48x48.png');
    tray.setToolTip('Smashladder Launcher');
    updateTray();



    tray.on('click', () => {
        mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show()
    })
    tray.on('double-click', function() {
        mainWindow.show();
    });


    var windowOptions = {
        width: 700,
        height: 800,
        resizable: false,
        icon: __dirname + '/images/android-icon-96x96.png'
    };
    if(constants.debugConsole)
    {
        windowOptions.width = 1200;
    }
    if (startMinimized == true){
        windowOptions.show = false;
    }
    mainWindow = new BrowserWindow(windowOptions);
    mainWindow.loadURL('file://' + __dirname + '/index.html');

    mainWindow.on('close', function (event) {
        if( !app.isQuiting){
            event.preventDefault();
            mainWindow.hide();
        }
        return false;
    });



    mainWindow.on('minimize',function(event){
        event.preventDefault();
        mainWindow.hide();
    });

    if(constants.debugConsole)
    {
        mainWindow.webContents.openDevTools();
    }

    mainWindow.on('closed', function() {
        mainWindow = null;
    });

    Files.mainWindow = mainWindow;
    SocketManager.setMainWindow(mainWindow);
    mainWindow.webContents.on('new-window', function(e){
        console.log('wat');
        e.preventDefault();
    });
    mainWindow.webContents.on('did-finish-load', function() {

        mainWindow.webContents.send('builds-loading');


        SocketManager.on('disableConnection', ()=>{
            mainWindow.webContents.send('requestAuthentication');
        });

        Authentication.load()
            .then(function(authentication){
                console.log('[RETRIEVED AUTHENTICATION]');
                mainWindow.webContents.send('authenticating');
                return authentication.checkAuthentication();
            }).then(function(authentication){
                loginReady(authentication.player);
                return true;
            }).catch(function(error){

                console.error('[AUTHENTICATION FAILED]', error);
                if(error)
                {
                    if(error.authentication_failure)
                    {
                        console.log('DELETING SESSION');
                        Authentication.deleteAuthentication(); //Session was bad
                    }
                    if(error.server_error)
                    {
                    }
                    if(error.no_player)
                    {
                    }
                }
                mainWindow.webContents.send('noPlayerToAuthenticate');
            });

    });

});

var statPuller;
var waitingForDolphin = false;
exports.openStatPuller = ()=>{
    if(statPuller)
    {
        waitingForDolphin = false;
        return;
    }
    waitingForDolphin = true;
    DolphinChecker.waitForDolphinToOpen().then((pid)=>{
        waitingForDolphin = false;
        console.log('OPENING PULLER');
        var directory = __dirname+'/puller.html';
        let windowOptions = {};
        if(constants.debugConsole)
        {
            windowOptions.show = true;
        }
        else
        {
            windowOptions.show = false;
        }
        statPuller = new BrowserWindow(windowOptions);
        statPuller.loadURL('file://' + directory);
        if(constants.debugConsole)
        {
            statPuller.webContents.openDevTools();
        }
        statPuller.on('close', function (event) {
            statPuller = null;
        });


        DolphinChecker.waitForDolphinToClose().then(()=>{
            exports.closeStatPuller();
        })

    });
};

exports.closeStatPuller = ()=>{
    if(statPuller)
    {
        waitingForDolphin = true;
        statPuller.webContents.send('closePuller');
        statPuller.destroy();
        statPuller = null;
    }
};

var disconnectSocketManager = exports.disconnectSocketManager = function(){
    console.log('hwee');
    SocketManager.disconnect();

};

exports.focus = function(){
    if(mainWindow)
    {
        mainWindow.focus();
    }
};
exports.minimize = function(){
    if(mainWindow)
    {
        mainWindow.minimize();
    }
};

var loginReady = exports.loginReady = function(player){
    Authentication.load()
        .then(function(authentication) {
            SocketManager.setAuthentication(authentication);
            SocketManager.connect(authentication);
            mainWindow.webContents.send('loginReady', authentication.player);
        }).catch(error=>{
            throw error;
        });
};
var changeLaunchAtStartup = function(value){
    value = !!value;
    console.log('changing startup options', value);

    var settings = getLoginItemSettingsDefaults();
    settings.openAtLogin = value;
    settings.openAsHidden = true;

    app.setLoginItemSettings(settings);

    console.log(getLoginItemSettings());
};

function getLoginItemSettingsDefaults(){
    const path = require('path');
    const appFolder = path.dirname(process.execPath);
    const updateExe = path.resolve(appFolder, '..', 'Update.exe');
    const exeName = path.basename(process.execPath);
    return {
        path: updateExe,
        args: [
            '--processStart', `"${exeName}"`,
            '--process-start-args', `"--hidden"`
        ]
    }
}
function getLoginItemSettings(){
    var defaults = getLoginItemSettingsDefaults();
    return app.getLoginItemSettings(defaults);
}
exports.app = app;
exports.selectDolphinLocation = function(){
    return Files.selectDolphinLocation(mainWindow);
};