"use strict";

var $ = null;
var jQuery = null;
jQuery = $ = require('./node_modules/jquery/dist/jquery.min.js');

const fetch = require('node-fetch');

const electron = require('electron');
const ipc = electron.ipcRenderer;
const shell = electron.shell;
const remote = electron.remote;
const mainProcess = remote.require('./index');

const constants = require("./components/constants");
const SmashladderApi = require("./components/SmashladderApi");

const Authentication = require("./components/Authentication.js");
const clipboard = electron.clipboard;

const Builds = require("./components/Builds.js");
const Notification= require("./components/Notification.js");


var ViewManager = require("./components/ViewManager");
const BuildLaunch = require("./components/BuildLaunchSmartDolphin");
const settings = require("./components/AppSettings");
ViewManager = new ViewManager($('#container'), $('#content'));


ipc.on('noPlayerToAuthenticate', function(event){
    ViewManager.changeView('login');
});
ipc.on('socketConnected', function(event){
    $('#logged_in').addClass('socket_connected').removeClass('socket_connecting');
});
ipc.on('socketDisconnected', function(event){
    $('#logged_in').removeClass('socket_connected').addClass('socket_connecting');
});
ipc.on('builds-loading', function(event){
   ViewManager.changeView('loading');
});
ipc.on('startNetplay', function(event, message){

    Builds.launchDolphinBuildById(message.data.dolphin_version.id, message, message.force_close ? true : false)
        .then((build) => {
            var successfulLaunches = settings.set('build');
        }).catch((error) =>{
            console.error('Issue launching build ' , error, message);
        });

});
ipc.on('hostNetplay', function(event, message){
    console.log(message);
    try{
        Builds.launchDolphinBuildById(message.data.dolphin_version.id, message, true);
    }
    catch(error){
        if(error && error.error)
        {
            Notification.showFocusedAlert("Error!", error.error);
        }
    }
});

ipc.on('startGame', function(event, message){
    Builds.startGame(message.data.dolphin_version.id);
});

ipc.on('quitDolphin', function(event){
    BuildLaunch.killChild()
        .then((response)=> {
        })
        .catch((error)=> {
            console.log('[QUIT ERROR?]');
        });
});

ipc.on('loginReady', function(event, player){
    $('#logged_in').addClass('active')
        .find('.username').text(player.username);
    ViewManager.changeView('dolphins');
});

ipc.on('requestAuthentication', function(event, buildName){

    Notification.requestAuthenticationPopup();
});

$('#request_authentication').on('click','.agree', function(e){
   mainProcess.loginReady(); 
});

var logoutButton = $('#logout_button');
logoutButton.on('click', function(e){

    if(logoutButton.hasClass('logging_out'))
    {
        return;
    }

    var finished = function(){
        $('#logged_in').removeClass('active');
        logoutButton.removeClass('logging_out');
        ViewManager.changeView('login');
        Authentication.deleteAuthentication();
        mainProcess.disconnectSocketManager();
    };
    logoutButton.addClass('logging_out');

    Authentication.load()
    .then(function(authentication){

        if(authentication)
        {
            var data = {};
            data.session_player_id = authentication.player.id;
            data.session_key = authentication.session.token;
            return SmashladderApi.post(constants.getUrl(constants.apiEndpoints.LOGOUT), data);
        }
        else
        {
            throw 'No authentication found';
        }
    }).then((response) => {
        var contentType = response.headers.get("content-type");
        if (contentType && contentType.indexOf("application/json") !== -1) {
            return response.json();
        }
        else {
            console.log(response.text());
            throw 'Did not get a json response 1';
        }
    }).then(function(response){
        finished();
    }).catch(function(error){
        finished();
    });

});

$(document).on('click', 'a[href^="http"]', function (event) {
    event.preventDefault();
    shell.openExternal(this.href);
});

