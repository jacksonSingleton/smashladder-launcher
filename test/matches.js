"use strict";

const {assert, expect, should} = require('chai');
const sinon = require('sinon');
const EventEmitter = require('events');
const MatchGame = require('../components/match/MatchGame.js');
const DolphinPlayer = require('../components/match/DolphinPlayer.js');
const LadderPlayer  = require('../components/match/LadderPlayer.js');
class FakeConnection extends EventEmitter {
    retrievePlayerStat(){}
}

describe('MatchGame with two players', function () {
    var sandbox;
    var connection;
    var retrieveStatStub;
    beforeEach(function() {
        var empty = function(){
        };
        var jQueryMethods = {
            clone: empty,
            findCache: empty,
            appendTo: empty,
            text: empty,
            html: empty,
            findCacheDisplayText: empty,
            addClass: empty,
            removeClass: empty,
            attr: empty,
            data: empty,
            on: empty,
            val: empty,
            change: empty,
            find: empty,
            remove: empty,
        };

        sandbox = sinon.sandbox.create();

        sandbox.stub(jQueryMethods, 'appendTo').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'findCache').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'findCacheDisplayText').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'clone').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'text').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'html').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'addClass').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'removeClass').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'attr').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'data').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'on').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'val').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'change').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'find').returns(jQueryMethods);
        sandbox.stub(jQueryMethods, 'remove').returns(jQueryMethods);

        connection = new FakeConnection();
        retrieveStatStub = sandbox.stub(connection, 'retrievePlayerStat');
        retrieveStatStub.withArgs('character', 1).returns(Promise.resolve('Pikachu'));
        retrieveStatStub.withArgs('character', 2).returns(Promise.resolve('Fox'));
        retrieveStatStub.withArgs('character', 3).returns(Promise.resolve(null));
        retrieveStatStub.withArgs('character', 4).returns(Promise.resolve(null));

        retrieveStatStub.withArgs('stocks', 1).returns(Promise.resolve(4));
        retrieveStatStub.withArgs('stocks', 2).returns(Promise.resolve(3));

        retrieveStatStub.withArgs('stage').returns(Promise.resolve("Battlefield"));

        MatchGame.setMatchTemplate(jQueryMethods);
        MatchGame.setMatchButtonTemplate(jQueryMethods);
        MatchGame.setPlayerTemplate(jQueryMethods);
        MatchGame.setMatchHolder(jQueryMethods);
        MatchGame.setMatchButtonHolder(jQueryMethods);
        MatchGame.setMenuHolder(jQueryMethods);
        MatchGame.setDolphinPlayerTemplate(jQueryMethods);
        MatchGame.setStatsContainer(jQueryMethods);

        MatchGame.setConnection(connection);

        sandbox.stub(MatchGame, 'postRetrieveReportMatchGame').returns(
            Promise.resolve({success:true})
        );

        sandbox.stub(MatchGame, 'postRetrieveMatchGameId').returns(
            Promise.resolve({
                match:{
                    game: {
                        id: 9000,
                    },
                    id:5000,
                    players:{
                        1:{
                            player:{
                                id:1,
                                username:'Anther'
                            },
                            match: {}
                        },
                        2:{
                            player:{
                                id:2,
                                username:'Jayzer'
                            },
                            match: {}
                        }
                    }
                },
            })
        );

        afterEach(function () {
            MatchGame.activeMatch = null;
            MatchGame.previousMatch = null;
            sandbox.restore();
            DolphinPlayer.playerElements = {};
        });

    });


    var defaultMatch = {
        game: {
            id: 'match_1',
            stage: 36,
            timeRemaining: 300,
        },
        players:{
            1:{
                character: 3,
                playerType: 'human',
            },
            2:{
                character: 5,
                playerType: 'human',
            }
        }
    };

    describe('MatchGame Process', function () {

        it('can parse dolphin player list string', function(){
           var string = `Anker[1] : 727(0b00f1f) Win | 1------- |
            Ping: 0ms
            Status: ready
            Jayker[2] : 727(0b00f1f) Win | -2------- |
            Ping: 0ms
            Status: ready
            `;
            
            var changedPlayers = DolphinPlayer.parseDolphinPlayerList(string);
            assert.equal('Anker', changedPlayers[1].getUsername());
            assert.equal('Jayker', changedPlayers[2].getUsername());

        });

        it('can set up a match in a basic state', function () {
            var match = MatchGame.manager(defaultMatch);

            assert.isOk(match, 'match should exist');

            assert.equal(match.id, 'match_1');

            assert.isOk(match.getStage());
            assert.isOk(match.getMatchPlayer(1), 'Player 1 is missing');
            assert.isOk(match.getMatchPlayer(2), 'Player 2 is missing');
            assert.isNotOk(match.getMatchPlayer(3), 'Player 3 is present');
            assert.isNotOk(match.getMatchPlayer(4), 'Player 4 is present');
        });

        it('can set up initial characters', function () {
            var match = MatchGame.manager(defaultMatch);
            assert.equal(match.getMatchPlayer(1).stats.character.name, 'Mr. Game & Watch');
            assert.equal(match.getMatchPlayer(2).stats.character.name, 'Bowser');
        });

        it('can set up initial characters with 0 id', function () {

            var currentMatch = clone(defaultMatch);
            currentMatch.players[1].character = 0;
            currentMatch.players[2].character = 0;
            var match = MatchGame.manager(currentMatch);

            assert.equal(match.getMatchPlayer(1).stats.character.name, 'Captain Falcon');
            assert.equal(match.getMatchPlayer(2).stats.character.name, 'Captain Falcon');
        });

        it('match players do not get replaced', function () {
            var match = MatchGame.manager(defaultMatch);

            var originalMatchPlayer = match.getMatchPlayer(1);
            assert.isOk(originalMatchPlayer);
            MatchGame.manager(defaultMatch);
            assert.equal(match.getMatchPlayer(1), originalMatchPlayer);
        });

        it('can assign a ladderPlayer to a matchPlayer', function(done){
            DolphinPlayer.setPlayer(1,'Jayzer');
            DolphinPlayer.setPlayer(2,'Anther');
            var match = MatchGame.manager(defaultMatch);
            match.on('retrievedLadderData', (response)=>{
                try{
                    assert.isOk(match.getMatchPlayer(1).ladderPlayer, 'Match player was assigned a ladder player');
                    assert.equal(match.getMatchPlayer(1).dolphinPlayer.getUsername(), match.getMatchPlayer(1).ladderPlayer.getUsername());
                    assert.equal(match.getMatchPlayer(2).dolphinPlayer.getUsername(), match.getMatchPlayer(2).ladderPlayer.getUsername());
                    assert.notEqual(match.getMatchPlayer(1).ladderPlayer.getUsername(), match.getMatchPlayer(2).ladderPlayer.getUsername());
                }catch(error){
                    return done(error);
                }
                return done();
            });
        });

        it('can set up dolphin player with mismatched players', function(done){
            DolphinPlayer.setPlayer(1,'Anther');
            DolphinPlayer.setPlayer(2,'L_Cancel');
            var match = MatchGame.manager(defaultMatch);

            match.on('retrievedLadderData', (response)=>{
                try{
                    assert.isOk(match.getMatchPlayer(1).ladderPlayer, 'Match player was assigned a ladder player');
                    assert.equal(match.getMatchPlayer(1).dolphinPlayer.getUsername(), match.getMatchPlayer(1).ladderPlayer.getUsername());
                    assert.isOk(match.getMatchPlayer(2).dolphinPlayer);
                    assert.isNotOk(match.getMatchPlayer(2).ladderPlayer);
                }catch(error){
                    return done(error);
                }
                return done();
            });

        });

        it('can serialize a match', (done)=>{
            DolphinPlayer.setPlayer(2,'Anther');
            DolphinPlayer.setPlayer(1,'Jayzer');
            var match = MatchGame.manager(defaultMatch);
            match.on('matchStarted', ()=>{
                try{
                    MatchGame.connection.emit('requestsFinished');
                }catch(error){
                    done(error);
                }
            });
            match.on('retrievedLadderData', ()=>{
                try{
                    MatchGame.postRetrieveReportMatchGame.restore();
                    sinon.stub(MatchGame,'postRetrieveReportMatchGame').callsFake((value)=>{
                        // console.log(value);
                        MatchGame.postRetrieveReportMatchGame.restore();
                        return done();
                    });
                    var data = match.reportToLadder();
                }catch(error){
                    MatchGame.postRetrieveReportMatchGame.restore();
                    return done(error);
                }
            });
        });


        it('assumes match is over when seconds reset', (done)=>{
            var matchData = clone(defaultMatch);
            matchData.game.timeRemaining = 300;
            var match = MatchGame.manager(matchData);

            assert.isOk(match);

            sandbox.stub(match, 'serializeForLadder').returns({rawr:'rawr'});
            setTimeout(function(){
                matchData.game.timeRemaining = 290;
                MatchGame.manager(matchData);

                setTimeout(function(){
                    matchData.game.timeRemaining = 350;
                    var newMatch = MatchGame.manager(matchData);
                },15);

            },15);
            match.on('matchFinished', ()=>{
               done();
            });
        });

        it('does not start a new match when counting', (done)=>{
            var lastMatch = null;
            for(var i = 10; i > 0; i--)
            {
                var seconds = Math.floor(i/2);
                var matchData = clone(defaultMatch);

                matchData.game.timeRemaining = seconds;

                var result = MatchGame.manager(matchData);
                if(lastMatch && lastMatch !== MatchGame.activeMatch)
                {

                    throw new Error('Expected same match to be the same, mismatch at ' + i);
                }
                lastMatch = MatchGame.activeMatch;
            }
            done();
        });


    });
    
});

function clone(obj) {
    if (obj === null || typeof(obj) !== 'object' || 'isActiveClone' in obj)
        return obj;

    if (obj instanceof Date)
        var temp = new obj.constructor(); //or new Date(obj);
    else
        var temp = obj.constructor();

    for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            obj['isActiveClone'] = null;
            temp[key] = clone(obj[key]);
            delete obj['isActiveClone'];
        }
    }

    return temp;
}