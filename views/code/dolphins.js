const $ = require('../../components/jqueryStuff');
var constants = require('../../components/constants');
var SmashladderApi = require('../../components/SmashladderApi.js');

const electron = require('electron');
var Builds = require("../../components/Builds.js");
const ipc = electron.ipcRenderer;
var remote = electron.remote;
const mainProcess = remote.require('./index');
const shiningStatsConnection = require('../../components/ShiningStatsConnection');
const MatchGame = require('../../components/match/MatchGame');
const Notification = require('../../components/Notification');

const LadderGame = require('../../components/LadderGame');

const dolphins = $('#dolphins');

const removeDolphinPopup = $('#remove_a_dolphin');
var hostCodeInputTemplate = null;
var allBuilds = null;
var buildElements = {};

var dolphinAddedTemplate = Builds.templateBuildElement = dolphins.find('.dolphin.template').removeClass('template').detach();

console.log('the games',LadderGame.getGames());
console.log('the games',LadderGame.getGames().size);
LadderGame.getGames().forEach((element)=>{
    dolphinAddedTemplate.findCache('select[name=game_name]').prepend(
        $('<option>').attr('value', element.getHint()).text(element.getTitle())
    );
});

Builds.buildsContainer = dolphins.find('.builds');

ipc.on('highlightBuild', function(event, buildName){
    allBuilds.highlightBuild(buildName);
});

ipc.on('joinGame', function(event, game){
    game = LadderGame.retrieve(game.ladder.id);
    var build = game.getPrimaryBuild(allBuilds);
    if(build)
    {
        let element = build.getElement();
        console.log('THE HINT', game.getHint());
        element.find('select[name=game_name]').val(game.getHint());
        element.find('.join').click();
        return;
    }
    throw new Error('Could not find a primary build');
});
ipc.on('hostGame', function(event, game){
    game = LadderGame.retrieve(game.ladder.id);
    var build = game.getPrimaryBuild(allBuilds);
    if(build)
    {
        let element = build.getElement();
        console.log('THE HINT', game.getHint());
        element.find('select[name=game_name]').val(game.getHint());
        element.find('.test-launch').click();
        return;
    }
    throw new Error('Could not find a primary build');
});

$('.dolphins_tabs')
    .on('dragstart', '.tab a',  function(e){
        e.preventDefault();
    })
    .tabs();

const statsContainer = $('#stats');

statsContainer.on('change', '#match_buttons', function(e){
    var button = $(this).find(':selected');
    $('#matches').find('.match_stats_display').removeClass('selected');
    button.data('matchElement').addClass('selected');
});

MatchGame.setMenuHolder($('#menu'));
MatchGame.setMatchHolder($('#matches'));
MatchGame.setMatchButtonHolder($('#match_buttons'));
MatchGame.setMatchButtonTemplate($('#match_buttons').find('.template').removeClass('template').attr('id','').remove());
MatchGame.setMatchTemplate(statsContainer.find('.match_stats_display.template').removeClass('template').remove());
MatchGame.setPlayerTemplate(statsContainer.find('.player_stats.template').removeClass('template').remove());
MatchGame.setDebugElement($('#stats_debugger'));
MatchGame.setJquery($);
MatchGame.setStatsContainer(statsContainer);

MatchGame.setConstants(constants);
MatchGame.setLadderApi(SmashladderApi);

MatchGame.setDolphinPlayerTemplate($('#dolphin_player_number').removeClass('template').remove());

var buildsLoaded = function(builds){
    var buildsContainer = dolphins.find('.builds');
    allBuilds = builds;
    var buildList = builds.getBuilds();
    var hasNewElementInList = false;
    for (var i in buildList) {
        if (!buildList.hasOwnProperty(i)) {
            continue;
        }
        var build = buildList[i];

        build.updateElement();
        if(build.hasNewElement)
        {
            hasNewElementInList = true;
        }
        build.hasNewElement = false;

    }
    if(hasNewElementInList)
    {
        Materialize.showStaggeredList('.builds');
    }
    dolphins.data('builds', builds);
};

var loadBuilds = function(){
    dolphins.addClass('loading');
    var finished = function(){
        dolphins.removeClass('loading');
    };
    var builds = Builds.retrieveActiveBuilds()
        .then(function(builds){
            buildsLoaded(builds);
            finished();
        }).then(()=>{
            allBuilds.syncToServer();
        }).catch(function(error){
            console.log('Builds not loaded', error);
            finished();
        });
};
loadBuilds();

function updateTestMode(enabled){
    if(enabled)
    {
        $('.builds').removeClass('production');
    }
    else
    {
        $('.builds').addClass('production');
    }
}
dolphins.on('change', '#test_mode', function(e){
    var checked = $(this).is(':checked');
    $('.dolphins_tabs .tab a:first').trigger('click');
    setTimeout(()=>{
        updateTestMode(checked);
    }, 250);

});

shiningStatsConnection.setStatsContainer(statsContainer);
shiningStatsConnection.setRemoteProcess(mainProcess, ipc);
shiningStatsConnection.connect();

if(shiningStatsConnection.getSettings().active){
    statsContainer.find('.stats_toggle').prop('checked', true);
    let settings = shiningStatsConnection.getSettings();
    settings.active = true;
    shiningStatsConnection.saveSettings(settings);
    shiningStatsConnection.connect();
}
else
{
    statsContainer.find('.stats_toggle').prop('checked', false);
    shiningStatsConnection.disconnect();
}


dolphins.on('change', '.switch input[name=stats_toggle]', function(e){
    var button = $(this);
    var isChecked = button.is(':checked');
    let settings = shiningStatsConnection.getSettings();

    if(isChecked)
    {
        settings.active = true;
        shiningStatsConnection.saveSettings(settings);
        shiningStatsConnection.connect();
    }
    else
    {
        settings.active = false;
        shiningStatsConnection.saveSettings(settings);
        shiningStatsConnection.disconnect();
    }
});

dolphins.on('change', '.switch input[name=build_toggle]', function(e){
    var button = $(this);

    var isChecked = button.is(':checked');
    var data = {
        active: isChecked?1:0,
        build_preference_id: button.data('build_preference_id')
    };
    var build = button.closest('.dolphin');

    build.data('build').active = isChecked;
    build.data('build').updateElement();


    build.addClass('working');
    button.addClass('disabled');
    button.prop('disabled', true);
    var finished = function(){
        button.removeClass('disabled').addClass('enabled');
        button.prop('disabled', false);
        build.removeClass('working');
    };
    var reset = function(){
        build.data('build').active = !isChecked;
        build.data('build').updateElement();
    };
   SmashladderApi.post(constants.getUrl(constants.apiEndpoints.UPDATE_BUILD_PREFERENCES), data)
       .then(response => {
           var contentType = response.headers.get("content-type");
           if (contentType && contentType.indexOf("application/json") !== -1) {
               return response.json();
           }
           else {

               return response.text().then((response)=>{
                    throw response;
                   return {success: false, has_cache: true, server_error: true};
               });
           }
       }).then(response=>{
           build.removeClass('working');
           if(!response.success)
           {
               reset();
               console.log(response);
               throw 'Something went wrong!';
           }

           var newBuilds = response.preferred_builds[2];
            var newBuildsOrganized = {};
            for(var newBuildKey in newBuilds)
            {
                if(!newBuilds.hasOwnProperty(newBuildKey))
                {
                    continue;
                }
                newBuildsOrganized[newBuilds[newBuildKey].id] = newBuilds[newBuildKey];
            }
           newBuilds = newBuildsOrganized;
            if(response.success)
            {
                var buildList = allBuilds.getBuilds();
                for(var i in buildList)
                {
                    if(!buildList.hasOwnProperty(i))
                    {
                        continue;
                    }
                    var currentBuild = buildList[i];
                    var replacement = newBuilds[currentBuild.id];
                    if(replacement)
                    {
                        var somethingChanged = currentBuild.update(replacement);
                        if(somethingChanged)
                        {
                            currentBuild.save();
                        }
                    }
                }
                buildsLoaded(allBuilds);
            }
       }).catch(function(error){
            console.log(error);
            reset();
            finished();
       });
});

DolphinViewActions  = {
    resetAllBuilds: () => {
        allBuilds.clearBuilds()
            .then(()=>{
                loadBuilds();
            });
    },
};
var resetAllDialog = $('#reset_all_dialog');
resetAllDialog.on('click', '.agree', DolphinViewActions.resetAllBuilds );
dolphins.on('click', '.edit_button', function(){
   dolphins.addClass('editing');
}).on('click', '.edit_end_button', function(){
    dolphins.removeClass('editing');
});

dolphins.on('click','.reset_all_button', function(){
    $('#reset_all_dialog').modal({
        dismissible: true,
        complete: function() {
        }
    }).modal('open');

}).on('click','.download', function(){
    let DolphinDownloader = require('../../components/DolphinDownloader');
    let StringManipulator = require('../../components/StringManipulator');
    let dolphinElement = $(this).closest('.dolphin');
    let progressIndicator = dolphinElement.find('.progress.static .determinate');
    let downloader = dolphinElement.findCache('.is_downloading');
    let speedText = dolphinElement.findCache('.speed');
    let percentText = dolphinElement.findCache('.percent_number');
    let downloadingText = dolphinElement.findCache('.downloading');
    let downloadStopper = dolphinElement.findCache('.stop-download');
    let statusText = dolphinElement.findCache('.is_downloading .status_text');
    dolphinElement.addClass('is_downloading');

    let build = dolphinElement.data('build');

    downloadingText.text('Downloading...');
    download = new DolphinDownloader(build, progressIndicator, statusText);
    let progress = download
        .startDownload(build.name)
        .on('progress', function (state) {
            if(state.percent && state.speed)
            {
                dolphinElement.addClass('has_speeds');
                progressIndicator.css('width', ""+(state.percent * 100) + '%');
                percentText.text( Math.round(state.percent * 100) );
                speedText.text((StringManipulator.humanFileSize(state.speed, true)) +'/s' );
            }
            else
            {
                dolphinElement.removeClass('has_speeds');
            }
        }).on('error',function(error){
            Notification.showFocusedAlert('Download Error', error);
            dolphinElement.removeClass('is_downloading has_speeds');
            console.log('error!');
            downloadStopper.data('request', null);
        }).on('end', function(){
            dolphinElement.addClass('is_unzipping has_speeds');
            console.log('ended!!');
            downloadStopper.data('request', null);
        });
    download.on('unzipped',(event, path)=>{
        
    });
    download.on('finished',()=>{
        dolphinElement.removeClass('is_downloading');
    });
    downloadStopper.data('request', download.request);

}).on('click','.stop-download', function(){
    if($(this).data('request'))
    {
        $(this).data('request').abort();
        $(this).data('request',null);
    }
}).on('click','.join', function(){
    var button = $(this);
    var parent = button.closest('.dolphin');

    var build = parent.data('build');
    if(!hostCodeInputTemplate)
    {
        hostCodeInputTemplate = $('#host_code_input')
            .detach()
            .attr('id','')
            .removeClass('template');
    }
    let hostCodeInput = hostCodeInputTemplate.clone();
    hostCodeInput.keypress((e)=>{
        if(e.which == 13){
            submit();
        }
    });

    let hostCodeInputTextArea = hostCodeInput.find('.host_code');
    let submit = function(){
        let hostCode = hostCodeInputTextArea.val();
        parent.find('.host_code').val(hostCode);
        parent.find('.test-launch').click();
    };

    hostCodeInputTextArea.on('keypress',()=>{
        hostCodeInputTextArea.removeClass('invalid');
    });

    let buttons = [
        {
            text: 'Join',
            onClick: (event)=>{
                console.log(event);
                if(hostCodeInputTextArea.val() == '')
                {
                    event.stopImmediatePropagation();
                    hostCodeInputTextArea.addClass('invalid');
                    return false;
                }
                submit();
            }
        },
        {
            text: 'Cancel'
        }
    ];
    let options = {
        dismissible: true,
        ready: ()=>{
            hostCodeInputTextArea[0].focus();
        }
    };
    Notification.showFocusedAlert('Enter Host Code', hostCodeInput, buttons, options);

}).on('click', '.select', function(){
    var element = $(this);
    var location = mainProcess.selectDolphinLocation();
    if(location)
    {
        var parent = element.closest('.dolphin');
        var input = parent.find('input[name=file_location]');
        input.removeClass('path_not_set invalid').val(location);

        var build = parent.data('build');

        build.setPath(location);
        build.updateElement();

        parent.data('build').test();

        allBuilds.save();
    }
}).on('click', '.remove', function(){

    var button = $(this);
    var parent = button.closest('.dolphin');


    var build = parent.data('build');
    build.clearPath();

    build.updateElement();

    build.save();


}).on('click', '.test-launch', function(){
    var launcher = $(this);
    var build = launcher.closest('.dolphin').data('build');
    var hostCodeElement = build.element.find('input[name=host_code]');
    if(!build.local_path)
    {
        hostCodeElement.addClass('invalid');
        return;
    }
    var hostCode = hostCodeElement.val();
    if(!hostCode)
    {
        hostCode = null;
    }

    launcher.prop('disabled', true);
    let finished = ()=>{
        launcher.prop('disabled', false);
    };
    var launched;
    if(hostCode)
    {
        launched = build.join(hostCode, true)
    }
    else
    {
        var gameLaunchInput = build.element.find('select[name=game_name]');
        var text = gameLaunchInput.find(':selected').text().trim();
        var gameLaunch = {
            launch: gameLaunchInput.val(),
            game: text
        };
        launched = build.host(gameLaunch, true);
    }
    launched
        .then(()=>{
            hostCodeElement.val('');
            finished();
        })
        .catch(()=>{
            finished();
        });

}).on('click', '.close', function(){
    var button = $(this);
    var build = button.closest('.dolphin').data('build');
    button.prop('disabled', true);
    setTimeout(()=>{
        button.prop('disabled', false);
    },1000);
    build.exit();
});
