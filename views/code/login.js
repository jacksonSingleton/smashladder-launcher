const $ = require('jquery');
const login = $('#login');
const electron = require('electron');
const remote = electron.remote;
const mainProcess = remote.require('./index');

var constants = require('../../components/constants.js');
var SmashladderApi = require("../../components/SmashladderApi.js");
var Authentication = require('../../components/Authentication');
var JsonHelper = require('../../components/JsonHelper');

var test1 = "rawr";
var test2 = {name:"rawr"};
var test3 = {name:{first:'anther',last:'sledge'}};
var test4 = {name:
    [{first:'anther',last:'sledge'},
        {first:'jayker',last:'slek'}]
    };

// console.log('here?');
// JsonHelper.flattenJSON(test1);
// JsonHelper.flattenJSON(test2);
// JsonHelper.flattenJSON(test3);
// JsonHelper.flattenJSON(test4);
//
// return;


login.on('submit', function(e){
    e.preventDefault();
    var inputs = login.find(':input');

    if(login.hasClass('loading'))
    {
        return;
    }

    inputs.prop('disabled', true);

    login.addClass('loading');
    var loading = login.find('.preloader-wrapper').addClass('active');


    var finished = function(){
        login.removeClass('loading');
        loading.removeClass('active');
        inputs.prop('disabled', false);
    };

    var data = {};
    data.username = $('#username').val();
    data.password = $('#password').val();

    SmashladderApi.post(constants.getUrl(constants.apiEndpoints.DOLPHIN_LAUNCHER_LOGIN), data, false)
        .then((response) => {
            var contentType = response.headers.get("content-type");
            if (contentType && contentType.indexOf("application/json") !== -1) {
                return response.json();
            }
            else {
                throw 'Did not get a json response 4';
            }
        })
        .then(function(response){
            console.log('[LOGIN RESPONSE]', response);
            if(response.success)
            {
                $('#password').val('');
                var authentication = new Authentication(response.player, response.session);
                return authentication.save().then(()=>{
                    return authentication;
                });
            }
            else
            {
                if(response.error)
                {
                    throw {error: response.error}
                }
                else
                {
                    throw {error:'Incorrect Username / Password!'};
                }
            }
        }).then((authentication)=>{
            mainProcess.loginReady(authentication.player);
            constants.player = authentication.player;
            finished();
        }).catch(function(error){
            console.trace(error);
            var errorMessageContainer = login.find('.error-message');
            if(error.error)
            {
                login.addClass('invalid');
                errorMessageContainer.text(error.error);
            }
            else
            {
                errorMessageContainer.text('Connection Issue');
            }
            finished();
        });
});

login.on('keyup', function(){
    login.removeClass('invalid');
});